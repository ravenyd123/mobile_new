﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.App;
using Android.Content;
using Android.OS;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Threading;
using Android.Graphics;
using AndroidApp = Android.App.Application;
using static Android.Resource;
using WellCompliance.View.Hazard;
using Android.Content.Res;
using Xamarin.Essentials;

namespace WellCompliance.Droid
{
    [Service(Exported = true, Name = "com.dis25.Service80")]
    public class PeriodicService80 : Service
    {

        Model.Timer TimerSend = new Model.Timer();
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Console.WriteLine("Начинаем80");

            var notification = new Notification.Builder(this)
                .SetContentTitle("WellCompliance " + AppInfo.VersionString)
                .SetContentText("Служба запущена")
                .SetSmallIcon(Resource.Drawable.ic_stat_name)
                .SetLargeIcon(BitmapFactory.DecodeResource(AndroidApp.Context.Resources, Resource.Drawable.icon))
                .SetOngoing(true)
                .SetChannelId("default")
                .Build();

            StartForeground(1451, notification);
            // StartForeground(101, TimerSend.AutoInquiry());
            new Task(() => {
                TimerSend.AutoInquiry();

                  Thread.Sleep(10000);

               }).Start();

    

            // MessagingCenter.Send<object, string>(this, "UpdateLabel", "Hello from Android");

            return StartCommandResult.Sticky;
        }



    }

}