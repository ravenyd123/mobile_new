﻿using System;
using Android.Content;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;

using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

using System;

using Android;
using Android.App;
using Android.Content.PM;
using Android.Locations;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;

//using Plugin.CurrentActivity;

namespace WellCompliance.Droid
{
    [Activity(Label = "WellCompliance",
        Icon = "@mipmap/icon", 
        Theme = "@style/MainTheme", 
        MainLauncher = true, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }


        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);


           // MyUtils.StartPowerSaverIntent(this);
            //  MyUtils.StartPowerSaverIntent(this);
            //   DependencyService.Register<ToastNotification>();
            //    ToastNotification.Init(this);
            // CrossCurrentActivity.Current.Init(this, bundle);
            //  CrossCurrentActivity.Current.Activity = this;

            //  testServiceIntent = new Intent(this.BaseContext, typeof(PeriodicService)); - удалить


            /* Оптимизация приложения под аккумулятор, чтобы на хайвее служба не выключалась для экономии энергии */
            //   Intent.SetAction(Android.Provider.Settings.ActionRequestIgnoreBatteryOptimizations);
            //  Intent.SetAction(Android.Provider.Settings.ActionIgnoreBatteryOptimizationSettings);

            LoadApplication(new App());
            CreateNotificationFromIntent(Intent);

            //   StartService(new Intent(this, typeof(PeriodicService)));
            //       StartForegroundService(new Intent(this, typeof(PeriodicService)));

            //      startServiceIntent = new Intent(this, typeof(ForegroundService));

            //  var intent = new Android.Content.Intent(this, new PeriodicService().Class);

            //  if (intent.Action == Intent.ActionBootCompleted)
            //  {
            //      Intent startIntent = new Intent(context, typeof(BootService));

               if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
                     StartForegroundService(new Intent(this, typeof(PeriodicService80)));
                 else
                      StartService(new Intent(this, typeof(PeriodicService)));

  
            // var alarmIntent = new Intent(this, typeof(BackgroundReceiver));

            //var pending = PendingIntent.GetBroadcast(this, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);

            // var alarmManager = GetSystemService(AlarmService).JavaCast<AlarmManager>();
            // alarmManager.Set(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + 3 * 1000, pending);
            // System.Console.WriteLine("Запуск службы");


        }

        private string getPackageName()
        {
            throw new NotImplementedException();
        }

        protected override void OnNewIntent(Intent intent)
        {
            CreateNotificationFromIntent(intent);
        }

        void CreateNotificationFromIntent(Intent intent)
        {
            if (intent?.Extras != null)
            {
                string title = intent.Extras.GetString(AndroidNotificationManager.TitleKey);
                string message = intent.Extras.GetString(AndroidNotificationManager.MessageKey);

                DependencyService.Get<INotificationManager>().ReceiveNotification(title, message);
            }
        }
    }
}