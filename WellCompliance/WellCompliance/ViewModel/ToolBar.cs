﻿using Xamarin.Forms;

namespace WellCompliance.ViewModel
{
    public class ToolBar : Constant
    {
        Model.Extensive extensive = new Model.Extensive();

        public ToolBar()
            : base()
        {
            Init();
        }
        /* Элементы ToolBar */
        private void Init()
        {
            ToolbarItem home = new ToolbarItem
            {
                Text = "Домой",
                Order = ToolbarItemOrder.Primary,
                Priority = 0,
                Icon = new FileImageSource
                {
                    File = "ico_home.png"
                }
            };

            ToolbarItem alert = new ToolbarItem
            {
                Text = "Уведомление",
                Order = ToolbarItemOrder.Primary,
                Priority = 1,
                Icon = new FileImageSource
                {
                    File = "ico_alert.png"
                }
            };

            // Элементы меню TabNav
            ToolbarItem profile = new ToolbarItem
            {
                Text = "Профиль",
                Order = ToolbarItemOrder.Secondary,
                Priority = 1,
            };

            ToolbarItem support = new ToolbarItem
            {
                Text = "Техническая поддержка",
                Order = ToolbarItemOrder.Secondary,
                Priority = 0
            };

            ToolbarItem settings = new ToolbarItem
            {
                Text = "Настройки",
                Order = ToolbarItemOrder.Secondary,
                Priority = 0,
            };

            ToolbarItem sync = new ToolbarItem
            {
                Text = "Синхронизация",
                Order = ToolbarItemOrder.Secondary,
                Priority = 3,
            };

            ToolbarItem help = new ToolbarItem
            {
                Text = "Помощь",
                Order = ToolbarItemOrder.Secondary,
                Priority = 4
            };

            ToolbarItem about = new ToolbarItem
            {
                Text = "О программе",
                Order = ToolbarItemOrder.Secondary,
                Priority = 5
            };

            ToolbarItem exit = new ToolbarItem
            {
                Text = "Выход",
                Order = ToolbarItemOrder.Secondary,
                Priority = 6,
            };
 
            // Отоброжаем элементы Tabnav
            ToolbarItems.Add(home);
            //ToolbarItems.Add(alert);
           // ToolbarItems.Add(about);
            ToolbarItems.Add(help);
            ToolbarItems.Add(settings);
            ToolbarItems.Add(exit);
           // ToolbarItems.Add(sync);
            //ToolbarItems.Add(support);
           // ToolbarItems.Add(profile);

            // На главную страницу
            home.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new View.MainMenuPage());
            };

            // Профиль
            profile.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new View.Profile());
            };

            // Техническая поддержка
            support.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new View.Support());
            };

            // Настройки
            settings.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new View.Settings());
            };

            // Синхронизация
            sync.Clicked += async (s, e) =>
            {
               
            };

            // Страница помощь
            help.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new View.Help());
            };

            // О программе
            about.Clicked += async (s, e) =>
            {
                await DisplayAlert("WC","Программа-клиент для работы в системе Well-Complance. Проведение аудитов, мониторинг за состоянием оборудования и прочее.", "Ok");
            };
            
            // Выход
            exit.Clicked += async (s, e) =>
            {
                bool status = await extensive.ClearProfileData();
                await DisplayAlert("Выход", "Вы вышли из системы", "Ok");
                await Navigation.PushAsync(new View.Authorization());
            };
        }
    }
}
