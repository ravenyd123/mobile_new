﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WellCompliance.ViewModel
{
    class Notification
    {

        // Класс заменен другим
        public async void DisplayMessage (string title, string text, string textbutton)
        {
            App.Current.MainPage.DisplayAlert(title, text, textbutton);
        }
    }
}
