﻿using Xamarin.Forms;

namespace WellCompliance.ViewModel
{
    public class ToolBarSmall : Constant
    {
        public ToolBarSmall()
            : base()
        {
            Init();
        }
        /* Элементы ToolBar */
        private void Init()
        {
            // Элементы меню TabNav
        //    ToolbarItem support = new ToolbarItem
        //    {
         //       Text = "Техническая поддержка",
         //       Order = ToolbarItemOrder.Secondary,
         //      Priority = 0
          //  };

            ToolbarItem settings = new ToolbarItem
            {
                Text = "Настройки",
                Order = ToolbarItemOrder.Secondary,
                Priority = 1,
            };

            ToolbarItem help = new ToolbarItem
            {
                Text = "Помощь",
                Order = ToolbarItemOrder.Secondary,
                Priority = 2
            };

            ToolbarItem about = new ToolbarItem
            {
                Text = "О программе",
                Order = ToolbarItemOrder.Secondary,
                Priority = 3
          };

            // Отоброжаем элементы Tabnav
          //  ToolbarItems.Add(about);
            ToolbarItems.Add(help);
            ToolbarItems.Add(settings);
        //    ToolbarItems.Add(support);

           
            // Техническая поддержка
          //  support.Clicked += async (s, e) =>
          //  {
          //      await Navigation.PushAsync(new View.Support());
          //  };

            // Настройки
            settings.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new View.Settings());
            };

          //  // Страница помощь
         //   help.Clicked += async (s, e) =>
         //   {
         //       await Navigation.PushAsync(new View.Help());
         //   };

            // О программе
         //   about.Clicked += async (s, e) =>
         //   {
        //        await DisplayAlert("WC", "Программа-клиент для работы в системе Well-Complance. Проведение аудитов, мониторинг за состоянием оборудования и прочее.", "Ok");
         //   };
        }
    }
}
