﻿using System;
using System.Collections.Generic;
using System.Text;
using WellCompliance.View.Hazard;
using Xamarin.Forms;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Specialized;


namespace WellCompliance.ViewModel
{
    class LocalNotifications : ContentPage
    {
        INotificationManager notificationManager;
        int notificationNumber = 0;

        public void CreateNotification(string title, string message, string type, int id)
        {
            Console.WriteLine("Цель1");
            notificationManager = DependencyService.Get<INotificationManager>();
            Console.WriteLine("Цель2");
            notificationManager.NotificationReceived += (sender, eventArgs) =>
            {
                Console.WriteLine("Цель3");
                var evtData = (NotificationEventArgs)eventArgs;
                Console.WriteLine("Цель4");
                ShowNotification(evtData.Title, evtData.Message, type, id);
                Console.WriteLine("Цель5");
            };
            notificationNumber++;
            Console.WriteLine("Цель6");
            notificationManager.ScheduleNotification(title, message);
            Console.WriteLine("Цель7");
        }

        public void ShowNotification(string title, string message, string type, int id)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
               // DependencyService.Get<INotificationManager>().Initialize();
               

                   if (type == "Responsible")
                   {
                    Application.Current.MainPage = new NavigationPage(new ResponsibleHazard(id));

                   }
                   if (type == "Executor")
                   {
                    Application.Current.MainPage = new NavigationPage(new ExecutorHazard(id));

                   }
                    if (type == "Iniciator_status")
                    {
                        Application.Current.MainPage = new NavigationPage(new IniciatorHazard(id));

                    }

                    if (type == "Responsible_status")
                    {
                        Application.Current.MainPage = new NavigationPage(new ResponsibleHazard(id));

                    }

                    if (type == "Executor_status")
                    {
                        Application.Current.MainPage = new NavigationPage(new ExecutorHazard(id));

                    }
                //  var nav = (Application.Current.MainPage as NavigationPage).Navigation;


            });
        
        }
    }
}
