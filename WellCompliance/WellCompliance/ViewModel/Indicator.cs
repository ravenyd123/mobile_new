﻿using Xamarin.Forms;

namespace WellCompliance.ViewModel
{
    class Indicator
    {
       

        public ActivityIndicator Icon(string TitleIndicator)
        {
            ActivityIndicator activityIndicator = new ActivityIndicator
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Fill,
                IsRunning = true,
                Color = Color.FromHex("#1E8141")
            };
            return activityIndicator;
        }
    }
}
