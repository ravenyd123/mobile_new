﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;
using System.Net.WebSockets;
using System.Net;
using System.Threading;
using WellCompliance.Model;
using System.Net.Http.Headers;
using System;
using System.IO;
using SQLite;
using Xamarin.Essentials;

namespace WellCompliance.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Authorization : ToolBarSmall
    {
        Model.Extensive extensive = new Model.Extensive();
        Model.LoadUserData loadUserData = new Model.LoadUserData();
        Model.SQL.Request request = new Model.SQL.Request();
        Model.Network network = new Model.Network();
        ViewModel.Indicator indicator = new ViewModel.Indicator();
        ViewModel.Notification notification = new ViewModel.Notification();
        Model.Timer TimerSend = new Model.Timer();
        Model.GetMyVersion getMyVersion = new GetMyVersion();
        string dbFileName = Path.Combine(FileSystem.AppDataDirectory, "wcbase.db3");

        public Authorization()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false); // Убираем стрелку назад
            var Connecting = request.BD();
            //  var Connecting = request.BD();
            var DataToBase = Connecting.Get<Model.SQL.Settings>(1);
            var CountBase = Connecting.Table<Model.SQL.Profile>().Count();

            if (CountBase != 0) { 
                var DataToBase2 = Connecting.Get<Model.SQL.Profile>(1);
                if (DataToBase.SaveMe == true)
                {
                    if ((DataToBase2.Login != null) && (DataToBase2.Password2 != null))
                    {
                        Login_pv.Text = DataToBase2.Login;
                        Password_pv.Text = DataToBase2.Password2;
                        Save_me.IsChecked = true;
                        inlet();
                    }
                }
            }
        }

       
        /* Автоматический вход при SaveMe=true и login+password в базе */
        public async void AutoInputBySaveMe()
        {
           //   Auth_button.IsEnabled = false;
            /* Включаем индикатор */
            //  var control = indicator.Icon("Авторизация");
            //   grid.Children.Add(control);
        }

        /* Переключатель показать/спрятать пароль */
        public void See_pass(object sender, EventArgs e)
        {
        }

        /* Действие при нажатии на кнопку ВОЙТИ */
        public async void Auth(object sender, EventArgs e)
        {
            inlet();
        }

            /* Действие при нажатии на кнопку ВОЙТИ */
        public async void inlet ()
        {
            var Connecting = request.BD();

            Auth_button.IsEnabled = false;
            /* Включаем индикатор */
            var control = indicator.Icon("Авторизация");
            grid.Children.Add(control);

            StringStatus.Text = "";
            Img_stop.IsVisible = false;
            if ((Login_pv.Text == null) || (Password_pv.Text == null))
            {
                grid.Children.Remove(control);
                notification.DisplayMessage(
                    "Авторизация",
                    "Логин или пароль не могут быть пустыми",
                    "Ok");
                Auth_button.IsEnabled = true;
            }
            else
            {
                // var connecting = request.BD();
                var Data = new NameValueCollection();
                string Pass_md5;
                Pass_md5 = await extensive.GetHashStringInMD5(Password_pv.Text);
                Pass_md5 = await extensive.GetHashStringInMD5(Pass_md5);
                string Url = network.GetUrlServer() + "/app/views/nearmiss/write.php";
                Data.Add("login", Login_pv.Text);
                Data.Add("password", Pass_md5);
                Data.Add("action", "Authorization");
                // Data.Add("action", "Authorization");
                var AnswerAuthorizationn = await network.DataTransfer(Url, Data);
                if (AnswerAuthorizationn == "0")
                {   /* Если логин или пароль неверные */
                    grid.Children.Remove(control);
                    StringStatus.TextColor = Color.Red;
                    StringStatus.Text = " Неверный логин или пароль";
                    Img_stop.IsVisible = true;
                    Login_pv.Text = null;
                    Password_pv.Text = null;
                    Auth_button.IsEnabled = true;
                }
                else if (AnswerAuthorizationn == "")
                {   /* Если ошибка при получении овтета с сервера*/
                    grid.Children.Remove(control);
                    notification.DisplayMessage(
                    "Авторизация",
                    "Проблемы с получением данных от сервера",
                    "Ok");
                    Auth_button.IsEnabled = true;
                }
                else
                {
                    /* Успешная авторизация */
                    var DataAnswer = await extensive.UserAccountJsonToData(AnswerAuthorizationn);
                    string UserName = DataAnswer.Name;
                    grid.Children.Remove(control);
                    /* Индикатор загрузка профиля */
                    control = indicator.Icon("Загрузка профиля");
                    grid.Children.Add(control);
                    bool StatusWriteUserAcountToBase = await extensive.UserAccountWriteToBase(DataAnswer);
                    if (StatusWriteUserAcountToBase)
                    {
                        var CountBase = Connecting.Table<Model.SQL.Profile>().Count();
                        if (Save_me.IsChecked = true)
                        {

                            Model.SQL.Request request = new Model.SQL.Request();
                            var Connecting34 = request.BD();
                            var DataToBase2678 = Connecting34.Get<Model.SQL.Profile>(1);
                            DataToBase2678.Password2 = Password_pv.Text;

                            Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(DataToBase2678);
                            });

                            var data3 = Connecting34.Get<Model.SQL.Settings>(1);

                            data3.SaveMe = true;
                            Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(data3);
                            });

                        }

                        bool StatusGetJob = await loadUserData.GetJob();
                        if (StatusGetJob)
                        {
                            bool StatusGetPlatforms = await loadUserData.GetPlatforms();
                            if (StatusGetPlatforms)
                            {
                                bool StatusGetActionDo = await loadUserData.GetActionDo();
                                if (StatusGetActionDo)
                                {
                                    await loadUserData.GetExecutorList();
                                    bool StatusGetStatusNearMiss = await loadUserData.GetStatusNearMiss();
                                    if (StatusGetStatusNearMiss)
                                    {

                                        ChekPrimary();
                                        ChekPrimaryExecutor();
                                        ChekPrimaryResponsible();
                                        StartMainPage();
                                    }
                                    else
                                    {
                                        grid.Children.Remove(control);
                                        Auth_button.IsEnabled = true;
                                        notification.DisplayMessage(
                                            "Загрузка профиля",
                                            "Ошибка получения статусов нарушения. Возможно нехватает памяти на мобильном устройстве или проблемы с полученными данными",
                                            "Ok");
                                    }
                                }
                                else
                                {
                                    grid.Children.Remove(control);
                                    Auth_button.IsEnabled = true;
                                    notification.DisplayMessage(
                                        "Загрузка профиля",
                                        "Ошибка получения списка действий инициатора. Возможно нехватает памяти на мобильном устройстве или проблемы с полученными данными",
                                        "Ok");
                                }
                            }
                            else
                            {
                                grid.Children.Remove(control);
                                Auth_button.IsEnabled = true;
                                notification.DisplayMessage(
                                    "Загрузка профиля",
                                    "Ошибка получения площадок. Возможно нехватает памяти на мобильном устройстве или проблемы с полученными данными",
                                    "Ok");
                            }
                        }
                        else
                        {
                            grid.Children.Remove(control);
                            Auth_button.IsEnabled = true;
                            notification.DisplayMessage(
                             "Загрузка профиля",
                             "Ошибка получения видов работ. Возможно нехватает памяти на мобильном устройстве или проблемы с полученными данными",
                             "Ok");
                        }

                    }
                    else
                    {
                        grid.Children.Remove(control);
                        Auth_button.IsEnabled = true;
                        notification.DisplayMessage(
                          "Загрузка профиля",
                          "Ошибка записи профиля. Возможно нехватает памяти на мобильном устройстве или проблемы с полученными данными",
                          "Ok");
                    }
                }
            }
        }

        public async void ChekPrimary()
        {
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.Iniciator>().Count();
            if (CountBase == 0)
            {
                PrimaryDLNM();
          
            }
        }

        public async void ChekPrimaryExecutor()
        {
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.Executor>().Count();
            if (CountBase == 0)
            {
                PrimaryDLNMExecutor();
            }
        
        }

        public async void ChekPrimaryResponsible()
        {
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.Responsible>().Count();
            if (CountBase == 0)
            {
                PrimaryDLNMResponsible();
            }
         
        }

        public async void PrimaryDLNM()
        {
            Model.PrimaryDloadNM primaryDloadNM = new Model.PrimaryDloadNM();
            primaryDloadNM.PrimaryDloadNearMissAsync();
           //loadUserData.GetTasks();
           // StartMainPage();
        }

        public async void PrimaryDLNMExecutor()
        {
            Model.PrimaryDloadNM primaryDloadNM = new Model.PrimaryDloadNM();
            primaryDloadNM.PrimaryDloadNearMissExecutorAsync();
            //loadUserData.GetTasks();
           // StartMainPage();
        }

        public async void PrimaryDLNMResponsible()
        {
            Model.PrimaryDloadNM primaryDloadNM = new Model.PrimaryDloadNM();
            primaryDloadNM.PrimaryDloadNearMissResponsibleAsync();
            //loadUserData.GetTasks();
           // StartMainPage();
        }

      
        /* Запускаем Main-страницу */
        public async void StartMainPage()
        {
            //TimerSend.AutoInquiry();
            await Navigation.PushAsync(new View.MainMenuPage());
        }
    }
}