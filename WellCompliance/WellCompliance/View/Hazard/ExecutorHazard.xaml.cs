﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Diagnostics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using WellCompliance.ViewModel;
using WellCompliance.Model;
using System.Collections.Specialized;

namespace WellCompliance.View.Hazard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class ExecutorHazard : ToolBar
    {
        Dictionary<int, string> PlatformsDictionary = new Dictionary<int, string>();
        Model.SQL.Request request = new Model.SQL.Request();
        Model.GetNameStatus getNameStatus = new GetNameStatus();
        SaveNearMissAndSendToServer saveNearMissAndSendToServer = new SaveNearMissAndSendToServer();
        ViewModel.Indicator indicator = new ViewModel.Indicator();
        private int nearId;
        Model.CompletingNM completingNM = new CompletingNM();
        Model.RemoveNM removeNM = new RemoveNM();
        Image img = new Image();
        string ImgFile;
        string ActionDoIniciator;
        public ExecutorHazard(int nearId)
        {
            this.nearId = nearId;
            var Connecting = request.BD();

            var Action = Connecting.Table<Model.SQL.ActionDo>();

            InitializeComponent();

            //   foreach (var lpk in Action)
            //   {
            //       PickerActionDo.Items.Add(lpk.Action); // Выводим в менюшке список того, что инициатор может сделать
            //  }
            /* если мы открыли для редактирования существующий near miss */

            if (nearId != -1)
            {
                StatusString.IsVisible = true; // Показываем строку с дополнительной информацией по near miss

                var DataToBase = Connecting.Get<Model.SQL.Executor>(nearId); // Созданные мною
                                                                                // var StatusName = Connecting.Get<Model.SQL.NearMissStatus>(DataToBase.Status);
                ActionIn.Text = DataToBase.Violation;
                ActionDoText.Text = DataToBase.ActionTaken;
                Platform.Text = DataToBase.Platform;

                WhatDo.Text = DataToBase.WhatDo;  
                Near_photo.Source = DataToBase.Photo;
                StatusNear.Text = getNameStatus.GetName(DataToBase.Status-1);
                DataCreate.Text = DataToBase.Date;
                Date.Text = DataToBase.DeadlineDate;
                Recomendations.Text = DataToBase.Recomendations;
                id.Text = DataToBase.Id.ToString();
                if (DataToBase.Status == 4) {
                    Yes.IsVisible = true;
                }

                if (DataToBase.Status == 8)
                {
                    Yes.IsVisible = true;
                }

                if (DataToBase.Status == 6)
                {
                    string type = "executor";
                    removeNM.Delete(type, nearId);

                }
            }



            //  Photo.Clicked += async (sender, args) =>
            //  {
            //      string action = await DisplayActionSheet("Выберите источник:", "Отмена", null, "Из галереи", "Сделать снимок");
            //       // Debug.WriteLine("Action: " + action);
            //       if (action == "Отмена") { return; }
            //       if (action == "Из галереи") { TakePhoto(); }
            //       if (action == "Сделать снимок") { DoPhoto(); }
            //   };

            var tapImages = new TapGestureRecognizer();

            tapImages.Tapped += async (s, e) =>
            {
                // Переводим тип Images.Resurce в string (адрес файла картинки)
                Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                ImgFile = objFileImageSource.File;

                await Navigation.PushAsync(new Photo(ImgFile));

            };
            Near_photo.GestureRecognizers.Add(tapImages);

           

        }

        /* Выбор что было сделано? */
        void PickerActionDo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //  Console.WriteLine(PickerActionDo.SelectedItem.ToString());
            //     if (PickerActionDo.SelectedItem.ToString() == "Другое:")
            //  {
            //       ActionDoText.IsVisible = true;
            //    }
            //    else
            //    {
            //      ActionDoText.IsVisible = false;
            //  }
        }

        /* Кнопка сохранить */
        //   async void SaveNear(object sender, EventArgs args)
        //  {
        //    var control = indicator.Icon("Сохранение");
        //     grid.Children.Add(control);
        //     var ResultDoSave = await DoSave();
        //      if (ResultDoSave)
        //     {
        //        grid.Children.Remove(control);
        //         await Navigation.PushAsync(new HazardIniciatorList());
        //    }

        // }


      //  public async void click2(object sender, EventArgs e)
     //   {
     //       string result = await DisplayPromptAsync("Не устранен?", "Укажите причину не устранения");
      //  }

        public async void click1(object sender, EventArgs e)
        {
           // InitializeComponent();
            string whatdoexecutor = WhatDo.Text;

            completingNM.CompliteSend(nearId, whatdoexecutor);
            DisplayAlert("Устранен", "Нарушение устранено", "OK");

            await Navigation.PushAsync(new ExecutorHazardList());

        }
        async void CloseNear3(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new ExecutorHazardList());

        }

    }

}