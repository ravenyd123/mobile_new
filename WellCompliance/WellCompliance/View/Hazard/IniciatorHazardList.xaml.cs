﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View.Hazard
{
    public class DateGroup2
    {
        public int nearid { get; set; }
        public int Id { get; set; }
        public int Status { get; set; }
        public string Date { get; set; }
        public string ImageSource { get; set; }
        public string ActionIn { get; set; }
        public string Violation { get; set; }
        public string StatusName { get; set; }
        public Color StatusColor { get; set; }

        // public string Platform { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IniciatorHazardList : ToolBar
    {
        public IniciatorHazardList()
        {
            var DateGroupList = new List<DateGroup2>();
            InitializeComponent();
            this.BackgroundImage = "phone.png";
            Model.SQL.Request request = new Model.SQL.Request();
            Model.GetNameStatus getNameStatus = new Model.GetNameStatus();
            Model.StatusToColor statusToColor = new Model.StatusToColor();
            var Connecting = request.BD();
            int nearid;


            var CountBase = Connecting.Table<Model.SQL.Iniciator>().Count();
            if (CountBase == 0)
            {
                InfoSW.Text = "Нет нарушений";
            }
            else
            {
                var DataToBase = Connecting.Table<Model.SQL.Iniciator>();
                foreach (var s in DataToBase)
                {
                   
                   
                    DateGroupList.Add(new DateGroup2
                    {
                        nearid = s.Id,
                        Id = s.IdServ,
                        Date = s.Date,
                        ActionIn = s.Violation,
                        StatusName = getNameStatus.GetName(s.Status - 1),
                        StatusColor = statusToColor.GetColor(s.Status)
                        //   Platform = s.Platform

                    }); 

                    MyListView.ItemsSource = DateGroupList;
                }
            }

          
        }


        /* Тапкаем по нарушению */
        async private void TapGestureRecognizer_Tapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            var dataItem = e.Item as DateGroup2;
            var selectedItem = e.ItemIndex;
            await Navigation.PushAsync(new IniciatorHazard(dataItem.nearid));


            //Удаляем селект
            ((ListView)sender).SelectedItem = null;
        }

        // При нажатии на Добавить
        async void Add_Hazard(object sender, EventArgs args)
        {
            int nearId = -1;
            await Navigation.PushAsync(new IniciatorHazard(nearId));

        }
    }
}