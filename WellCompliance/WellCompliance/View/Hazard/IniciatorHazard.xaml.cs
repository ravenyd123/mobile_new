﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Plugin.AudioRecorder;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Diagnostics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using WellCompliance.ViewModel;
using WellCompliance.Model;
using System.Collections.Specialized;

namespace WellCompliance.View.Hazard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class IniciatorHazard : ToolBar
    {
        Dictionary<int, string> PlatformsDictionary = new Dictionary<int, string>();
        Model.SQL.Request request = new Model.SQL.Request();
        SaveNearMissAndSendToServer saveNearMissAndSendToServer = new SaveNearMissAndSendToServer();
        ViewModel.Indicator indicator = new ViewModel.Indicator();
        Model.GetNameStatus getNameStatus = new GetNameStatus();
        Model.RemoveNM removeNM = new RemoveNM();
        private int nearId;
        Image img = new Image();
        string ImgFile;
        string ActionDoIniciator;

        public IniciatorHazard(int nearId)
        {
            
            this.nearId = nearId;
            var Connecting = request.BD();
            var Platforms = Connecting.Table<Model.SQL.Platforms>();
            var Action = Connecting.Table<Model.SQL.ActionDo>();

            InitializeComponent();

            foreach (var l in Platforms)
            {
                PickerPlatform.Items.Add(l.PlatformsName);
            }

            //   foreach (var lpk in Action)
            //   {
            //       PickerActionDo.Items.Add(lpk.Action); // Выводим в менюшке список того, что инициатор может сделать
            //  }
            /* если мы открыли для редактирования существующий near miss */

            if (nearId != -1)
           {
                StatusString.IsVisible = true; // Показываем строку с дополнительной информацией по near miss
               
                var DataToBase = Connecting.Get<Model.SQL.Iniciator>(nearId); // Созданные мною
                                                                              // var StatusName = Connecting.Get<Model.SQL.NearMissStatus>(DataToBase.Status);
                if (DataToBase.IdServ==null)
                {
                    id.IsVisible = false;
                    id.Text = "Нет";
                } else
                {
                    id.Text = DataToBase.IdServ.ToString();
                }
                    ActionIn.Text = DataToBase.Violation;
               ActionDoText.Text = DataToBase.ActionTaken;
              Near_photo.Source = DataToBase.Photo;
               StatusNear.Text = getNameStatus.GetName(DataToBase.Status-1);
               DataCreate.Text = DataToBase.Date;
              //  id.Text = DataToBase.Id.ToString();
                PickerPlatform.SelectedIndex = DataToBase.Platform;

                if(DataToBase.Status > 2)
                {
                    PickerPlatform.IsEnabled = false;
                    ActionIn.IsEnabled = false;
                    ActionDoText.IsEnabled = false;
                    BtSave.IsEnabled = false;
                    Photo.IsEnabled = false;
                }

                if (DataToBase.Status == 6)
                {
                    string type = "iniciator";
                    removeNM.Delete(type, nearId);

                }
            }



            Photo.Clicked += async (sender, args) =>
            {
                string action = await DisplayActionSheet("Выберите источник:", "Отмена", null, "Из галереи", "Сделать снимок");
                // Debug.WriteLine("Action: " + action);
                if (action == "Отмена") { return; }
                if (action == "Из галереи") { TakePhoto(); }
                if (action == "Сделать снимок") { DoPhoto(); }
            };

            async void TakePhoto()
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    await CrossMedia.Current.Initialize();

                    if (CrossMedia.Current.IsPickPhotoSupported)
                    {
                        MediaFile photo = await CrossMedia.Current.PickPhotoAsync();
                        if (photo == null)
                        {
                            return;
                        }


                        img.Source = ImageSource.FromFile(photo.Path);

                        Near_photo.Source = img.Source;

                        // for ( int ndx = 0; ndx <6; ndx++)
                        //    {
                        //   ImagesList.Add(new ImagesDates
                        //   {

                        // Patch = img.Source
                        //       ImagePatch = "11111"
                        //    });

                        //   }


                    }
                    else
                    {
                        DisplayAlert("Работа с фотографиями не поддерживается", "Нет доступа к фотографиям", "OK");
                        return;
                    }
                    // MyListView.ItemsSource = ImagesList;
                    // Near_photo.Source = img.Source;
                    // popo.Source = img.Source;


                }
                else
                {
                    await DisplayAlert("Доступ закрыт", "Невозможно выбрать фотографию", "OK");
                    //On iOS you may want to send your user to the settings screen.
                    //CrossPermissions.Current.OpenAppSettings();
                }

            };

            async void DoPhoto()
            {

                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    await CrossMedia.Current.Initialize();

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        DisplayAlert("Нет камеры", "Камера не найдена", "OK");
                        return;
                    }

                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "WellCompliance",
                        SaveToAlbum = true,
                        CompressionQuality = 75,
                        CustomPhotoSize = 50,
                        PhotoSize = PhotoSize.MaxWidthHeight,
                        MaxWidthHeight = 2000,
                        DefaultCamera = CameraDevice.Rear
                    });

                    if (file == null)
                        return;

                    //  DisplayAlert("File Location", file.Path, "OK");

                    img.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        file.Dispose();
                        return stream;
                    });
                    Near_photo.Source = file.Path;
                }

            };


            var tapImages = new TapGestureRecognizer();

            tapImages.Tapped += async (s, e) =>
            {
                // Переводим тип Images.Resurce в string (адрес файла картинки)
                Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                ImgFile = objFileImageSource.File;

                await Navigation.PushAsync(new Photo(ImgFile));

            };
            Near_photo.GestureRecognizers.Add(tapImages);


        }

        /* Выбор что было сделано? */
        void PickerActionDo_SelectedIndexChanged(object sender, EventArgs e)
        {
          //  Console.WriteLine(PickerActionDo.SelectedItem.ToString());
       //     if (PickerActionDo.SelectedItem.ToString() == "Другое:")
          //  {
         //       ActionDoText.IsVisible = true;
        //    }
        //    else
        //    {
          //      ActionDoText.IsVisible = false;
          //  }
        }

        /* Кнопка сохранить */
        async void SaveNear(object sender, EventArgs args)
        {
            var control = indicator.Icon("Сохранение");
            grid.Children.Add(control);
            var ResultDoSave = await DoSave();
            if (ResultDoSave)
            {
                grid.Children.Remove(control);
                await Navigation.PushAsync(new IniciatorHazardList());
            }

        }


        /* Само сохранение */
        async Task<bool> DoSave()
        {
       //     if (PickerActionDo.SelectedItem.ToString() == "Другое:")
        //    {
        //        ActionDoIniciator = ActionDoText.Text;
        //    }
        //    else
        //    {
        //        ActionDoIniciator = PickerActionDo.SelectedItem.ToString();
        //    }


        //    if ((PickerJob.SelectedItem == null) || (ActionIn.Text == "") || (PickerPlatform.SelectedItem == null))
          //  {
        //        await DisplayAlert("", "Не все обязательные поля были заполнены", "Ok");
        //    }
        //    else
       //     {
                Model.SQL.Request request = new Model.SQL.Request();
                var Connecting = request.BD();
                string ImgFile;
                if (this.nearId != -1)
                {
                    if (Near_photo.Source != null)
                    {
                        Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                        ImgFile = objFileImageSource.File;
                    }
    else
                    {
                        ImgFile = "";
                    }
                    var DataToBase = Connecting.Get<Model.SQL.Iniciator>(nearId);
                    DataToBase.Violation = ActionIn.Text;
                DataToBase.ActionTaken = ActionDoText.Text;
                    DataToBase.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    DataToBase.Photo = ImgFile;
                DataToBase.Platform = PickerPlatform.SelectedIndex;

                Connecting.RunInTransaction(() =>
                    {
                        Connecting.Update(DataToBase);
                    });
                }
                else
               {
                    if (Near_photo.Source != null)
                    {
                        Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                       ImgFile = objFileImageSource.File;
                    }
                    else
                    {
                        ImgFile = "";
                    }

                    var newData = new Model.SQL.Iniciator
                    {
                        Violation = ActionIn.Text,
                        ActionTaken = ActionDoText.Text,
                        Status = 1,
                        Platform = PickerPlatform.SelectedIndex,


                Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),

                     
                       Photo = ImgFile
                   };

                    Connecting.Insert(newData);
                }
                var ResultSendToServer = await saveNearMissAndSendToServer.SendToServer2(nearId);
                if (ResultSendToServer)
                {
                  return true;
                }
            
            return true;
        }









        /* Закрыть нарушение */
        async void CloseNear(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new IniciatorHazardList());

        }




    }

}