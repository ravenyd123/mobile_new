﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Diagnostics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using WellCompliance.ViewModel;
using WellCompliance.Model;
using System.Collections.Specialized;

namespace WellCompliance.View.Hazard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class ResponsibleHazard : ToolBar
    {
        Dictionary<int, string> PlatformsDictionary = new Dictionary<int, string>();
        Model.SQL.Request request = new Model.SQL.Request();
        Model.GetNameStatus getNameStatus = new GetNameStatus();
        SaveNearMissAndSendToServer saveNearMissAndSendToServer = new SaveNearMissAndSendToServer();
        ViewModel.Indicator indicator = new ViewModel.Indicator();
        ViewModel.Notification notification = new ViewModel.Notification();
        Model.CompletingNM completingNM = new CompletingNM();
        Model.RemoveNM removeNM = new RemoveNM();
        private int nearId;
        Image img = new Image();
        string ImgFile;
        string ActionDoIniciator;
        int IDEx;
        string date;
        DateTime myDate;
        public ResponsibleHazard(int nearId)
        {
            this.nearId = nearId;
            var Connecting = request.BD();

            var Action = Connecting.Table<Model.SQL.ActionDo>();
            var ExecutorList = Connecting.Table<Model.SQL.ExecuterList>();
            
            InitializeComponent();

            foreach (var lpk in ExecutorList)
               {
                PickerExecutorList.Items.Add(lpk.Fio); // Список исполнителей
               }





                //   foreach (var lpk in Action)
                //   {
                //       PickerActionDo.Items.Add(lpk.Action); // Выводим в менюшке список того, что инициатор может сделать
                //  }
                /* если мы открыли для редактирования существующий near miss */

                if (nearId != -1)
            {
                StatusString.IsVisible = true; // Показываем строку с дополнительной информацией по near miss

                var DataToBase = Connecting.Get<Model.SQL.Responsible>(nearId); // Созданные мною
                var idExecutor = Connecting.Query<Model.SQL.ExecuterList>("SELECT `Id` FROM `ExecuterList` WHERE `ServerId`=" + DataToBase.Executor + "");
                IDEx = - 1;
                foreach (var b in idExecutor)
                {
                    IDEx = b.Id-1;   
                } 

                ActionIn.Text = DataToBase.Violation;
                    ActionDoText.Text = DataToBase.ActionTaken;
                    Platform.Text = DataToBase.Platform;
                    Near_photo.Source = DataToBase.Photo;
                    StatusNear.Text = getNameStatus.GetName(DataToBase.Status - 1);
                    DataCreate.Text = DataToBase.Date;
                    DataCreate2.Text = DataToBase.Date;
                    Iniciator.Text = DataToBase.Iniciator;
                    id.Text = DataToBase.Id.ToString();
                    PickerExecutorList.SelectedIndex = IDEx;
                    Recomm.Text = DataToBase.Recomendation;
                whatdo.Text = DataToBase.WhatDo;

                if (DataToBase.Status == 5)
                {
                    ButtonSoglas.IsVisible = true;
                    ButtonNotSoglas.IsVisible = true;
                    buttonSave.IsVisible = false;
                }

                if (DataToBase.Status == 6)
                {
                    buttonSave.IsVisible = false;
                    DatePickerDeadLine.IsEnabled = false;
                    PickerExecutorList.IsEnabled = false;
                    Recomm.IsEnabled = false;
                }

                if (DataToBase.DeadlineDate == null)
                {
                    string dt = DateTime.Now.ToString("dd.MM.yyyy");
                    myDate = DateTime.ParseExact(dt, "dd.MM.yyyy",
                                             System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                        myDate = DateTime.ParseExact(DataToBase.DeadlineDate, "dd.MM.yyyy",
                                              System.Globalization.CultureInfo.InvariantCulture);
         
                }
                DatePickerDeadLine.Date = myDate;

                if (DataToBase.Status == 6)
                {
                    string type = "responsible";
                    removeNM.Delete(type, nearId);

                }
                //  DatePickerDeadLine. = DataToBase.DeadlineDate;
            }



            //  Photo.Clicked += async (sender, args) =>
            //  {
            //      string action = await DisplayActionSheet("Выберите источник:", "Отмена", null, "Из галереи", "Сделать снимок");
            //       // Debug.WriteLine("Action: " + action);
            //       if (action == "Отмена") { return; }
            //       if (action == "Из галереи") { TakePhoto(); }
            //       if (action == "Сделать снимок") { DoPhoto(); }
            //   };
            var tapImages = new TapGestureRecognizer();

            tapImages.Tapped += async (s, e) =>
            {
                // Переводим тип Images.Resurce в string (адрес файла картинки)
                Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                ImgFile = objFileImageSource.File;

                await Navigation.PushAsync(new Photo(ImgFile));

            };
            Near_photo.GestureRecognizers.Add(tapImages);


        }

        /* Выбор что было сделано? */
        void PickerActionDo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //  Console.WriteLine(PickerActionDo.SelectedItem.ToString());
            //     if (PickerActionDo.SelectedItem.ToString() == "Другое:")
            //  {
            //       ActionDoText.IsVisible = true;
            //    }
            //    else
            //    {
            //      ActionDoText.IsVisible = false;
            //  }
        }

        /* Кнопка сохранить */
        async void click1(object sender, EventArgs args)
        {
            if (PickerExecutorList.SelectedIndex == -1)
            //|| (Password_pv.Text == null))
            {
                notification.DisplayMessage(
                    "Сохранение",
                    "Поле исполнителя не может быть пустым",
                    "Ok");
            }
            else
            {
                var control = indicator.Icon("Сохранение");
                grid.Children.Add(control);
                var ResultDoSave = await DoSave("");
                if (ResultDoSave)
                {
                    grid.Children.Remove(control);
                    await Navigation.PushAsync(new Hazard.ResponsibleHazardList());
                }
            }
        }

        
        /* Кнопка не согл */
        async void clickNotSoglas(object sender, EventArgs args)
        {

            string result = await DisplayPromptAsync("Укажите причину не согласования","");
            DisplayAlert("Согласование", "Не согласовано. NearMiss возвращен в работу", "OK");

            var control = indicator.Icon("Сохранение");
                grid.Children.Add(control);
                var ResultDoSave = await DoSave(result);
                if (ResultDoSave)
                {
                    grid.Children.Remove(control);
                    await Navigation.PushAsync(new Hazard.ResponsibleHazardList());
                }
            
        } 




        /* Само сохранение */
        async Task<bool> DoSave(string reason)
        {
            //     if (PickerActionDo.SelectedItem.ToString() == "Другое:")
            //    {
            //        ActionDoIniciator = ActionDoText.Text;
            //    }
            //    else
            //    {
            //        ActionDoIniciator = PickerActionDo.SelectedItem.ToString();
            //    }


            //    if ((PickerJob.SelectedItem == null) || (ActionIn.Text == "") || (PickerPlatform.SelectedItem == null))
            //  {
            //        await DisplayAlert("", "Не все обязательные поля были заполнены", "Ok");
            //    }
            //    else
            //     {
            Model.SQL.Request request = new Model.SQL.Request();
            var Connecting = request.BD();
            string ImgFile;
            int Pickerchan = PickerExecutorList.SelectedIndex + 1;
            var idExecutorServer = Connecting.Query<Model.SQL.ExecuterList>("SELECT `ServerId` FROM `ExecuterList` WHERE `Id`=" + Pickerchan + "");
            foreach (var b in idExecutorServer)
            {
                    var DataToBase = Connecting.Get<Model.SQL.Responsible>(nearId);
                    if (DatePickerText.Text ==null)
                {
                   date = DateTime.Today.ToString("dd/MM/yyyy");
                } else
                {
                   date = DatePickerText.Text;
                }

                    DataToBase.DeadlineDate = date;
                    DataToBase.Executor = b.ServerId;
                    DataToBase.Recomendation = Recomm.Text;
                    DataToBase.Status = 4;
                DataToBase.ReasonOfInconsistency = reason;

                    Connecting.RunInTransaction(() =>
                    {
                        Connecting.Update(DataToBase);
                    });
               
            }
            var ResultSendToServer = await saveNearMissAndSendToServer.SendToServer3(nearId);
            if (ResultSendToServer)
            {
                return true;
            }

            return true;
        }

        private void OnDateSelected(object sender, DateChangedEventArgs e)
        {
            if (DatePickerText != null)
                DatePickerText.Text = e.NewDate.ToString("dd/MM/yyyy");
        }


        /* Кнопка Согласования */
        async void clickSoglas(object sender, EventArgs args)
        {

            completingNM.SoglasSend(nearId);
            DisplayAlert("Согласование", "Выполнение нарушения согласовано", "OK");

            await Navigation.PushAsync(new ResponsibleHazardList());

        }


            /* Закрыть нарушение */
            async void CloseNear1(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new ResponsibleHazardList());

        }




    }

}