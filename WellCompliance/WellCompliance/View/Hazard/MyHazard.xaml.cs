﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Diagnostics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using WellCompliance.ViewModel;
using WellCompliance.Model;
using System.Collections.Specialized;

namespace WellCompliance.View.Hazard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class MyHazard : ToolBar
    {
        Dictionary<int, string> PlatformsDictionary = new Dictionary<int, string>();
        Model.SQL.Request request = new Model.SQL.Request();
        SaveNearMissAndSendToServer saveNearMissAndSendToServer = new SaveNearMissAndSendToServer();
        ViewModel.Indicator indicator = new ViewModel.Indicator();
        private int nearId;
        Image img = new Image();
        string ImgFile;
        string ActionDoIniciator;

        public MyHazard(int nearId)
        {
            this.nearId = nearId;
            var Connecting = request.BD();
            var Platforms = Connecting.Table<Model.SQL.Platforms>();
            var Jobs = Connecting.Table<Model.SQL.Jobs>();
            var Action = Connecting.Table<Model.SQL.ActionDo>();
            var PlatformsList = new NameValueCollection();


            InitializeComponent();
            
            foreach (var l in Platforms)
            {
                PickerPlatform.Items.Add(l.PlatformsName);
            }
            foreach (var lp in Jobs)
            {
                PickerJob.Items.Add(lp.JobsName); // Выодим в менюшке список работ
            }
            foreach (var lpk in Action)
            {
                PickerActionDo.Items.Add(lpk.Action); // Выводим в менюшке список того, что инициатор может сделать
            }
            /* если мы открыли для редактирования существующий near miss */

            if (nearId != -1)
            {
                StatusString.IsVisible = true; // Показываем строку с дополнительной информацией по near miss

                var DataToBase = Connecting.Get<Model.SQL.NearMissMy>(nearId); // Созданные мною
                var StatusName = Connecting.Get<Model.SQL.NearMissStatus>(DataToBase.Status);
                ActionIn.Text = DataToBase.Violation;
                ActionDoText.Text = DataToBase.ActionTaken;
                Near_photo.Source = DataToBase.Photo;
                StatusNear.Text = StatusName.NameStatus;
                DataCreate.Text = DataToBase.Date;
                id.Text = DataToBase.Id.ToString();
                // CommentPlatform.Text = DataToBase.PlatformComment;
                //characteristicsNM.SelectedIndex = DataToBase.CharacteristicsNM;
                PickerJob.SelectedIndex = 3;
                PickerPlatform.SelectedIndex = DataToBase.Platform;

            }



                Photo.Clicked += async (sender, args) =>
                {
                    string action = await DisplayActionSheet("Выберите источник:", "Отмена", null, "Из галереи", "Сделать снимок");
                    // Debug.WriteLine("Action: " + action);
                    if (action == "Отмена") { return; }
                    if (action == "Из галереи") { TakePhoto(); }
                    if (action == "Сделать снимок") { DoPhoto(); }
                };

                async void TakePhoto()
                {
                    var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                    var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                        cameraStatus = results[Permission.Camera];
                        storageStatus = results[Permission.Storage];
                    }

                    if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                    {
                        await CrossMedia.Current.Initialize();

                        if (CrossMedia.Current.IsPickPhotoSupported)
                        {
                            MediaFile photo = await CrossMedia.Current.PickPhotoAsync();
                            if (photo == null)
                            {
                                return;
                            }

                            img.Source = ImageSource.FromFile(photo.Path);
                        }
                        else
                        {
                            DisplayAlert("Работа с фотографиями не поддерживается", "Нет доступа к фотографиям", "OK");
                            return;
                        }
                        Near_photo.Source = img.Source;

                        //   }
                        //    else if (status != PermissionStatus.Unknown)
                        //   {
                        //       await DisplayAlert("Камера запрещена", "Невозможно продолжить, измените настройки доступа к камере", "OK");
                        //    }
                    }
                    else
                    {
                        await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
                        //On iOS you may want to send your user to the settings screen.
                        //CrossPermissions.Current.OpenAppSettings();
                    }


                };

                async void DoPhoto()
                {

                    var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                    var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                        cameraStatus = results[Permission.Camera];
                        storageStatus = results[Permission.Storage];
                    }

                    if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                    {
                        await CrossMedia.Current.Initialize();

                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            DisplayAlert("Нет камеры", "Камера не найдена", "OK");
                            return;
                        }

                        var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            Directory = "WellCompliance",
                            SaveToAlbum = true,
                            CompressionQuality = 75,
                            CustomPhotoSize = 50,
                            PhotoSize = PhotoSize.MaxWidthHeight,
                            MaxWidthHeight = 2000,
                            DefaultCamera = CameraDevice.Rear
                        });

                        if (file == null)
                            return;

                        //  DisplayAlert("File Location", file.Path, "OK");

                        img.Source = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                            file.Dispose();
                            return stream;
                        });
                        Near_photo.Source = file.Path;
                    }

                };


                var tapImages = new TapGestureRecognizer();

                tapImages.Tapped += async (s, e) =>
                {
                    // Переводим тип Images.Resurce в string (адрес файла картинки)
                    Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                    ImgFile = objFileImageSource.File;

                    await Navigation.PushAsync(new Photo(ImgFile));

                };
                Near_photo.GestureRecognizers.Add(tapImages);

   
        }

        /* Выбор что было сделано? */
        void PickerActionDo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine(PickerActionDo.SelectedItem.ToString());
            if (PickerActionDo.SelectedItem.ToString() == "Другое:")
            {
                ActionDoText.IsVisible = true;
            }
            else
            {
                ActionDoText.IsVisible = false;
            }
        }

        /* Кнопка сохранить */
        async void SaveNear(object sender, EventArgs args)
        {
            var control = indicator.Icon("Сохранение");
            grid.Children.Add(control);
            var ResultDoSave = await DoSave();
            if (ResultDoSave)
            {
                grid.Children.Remove(control);
                await Navigation.PushAsync(new MyHazardsList());
            }
            
        }



        /* Само сохранение */
        async Task<bool> DoSave()
        {
            if (PickerActionDo.SelectedItem.ToString() == "Другое:")
            {
                ActionDoIniciator = ActionDoText.Text;
            }
            else
            {
                ActionDoIniciator = PickerActionDo.SelectedItem.ToString();
            }


            if ((PickerJob.SelectedItem == null) || (ActionIn.Text == "") || (PickerPlatform.SelectedItem == null))
            {
                await DisplayAlert("", "Не все обязательные поля были заполнены", "Ok");
            }
            else
            {
                Model.SQL.Request request = new Model.SQL.Request();
                var Connecting = request.BD();
                string ImgFile;
                if (this.nearId != -1)
                {
                    if (Near_photo.Source != null)
                    {
                        Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                        ImgFile = objFileImageSource.File;
                    }
                    else
                    {
                        ImgFile = "";
                    }
                    var DataToBase = Connecting.Get<Model.SQL.NearMissMy>(nearId);
                    DataToBase.Violation = ActionIn.Text;
                    DataToBase.Job = PickerJob.SelectedIndex;
                    //DataToBase.Date = DateNear.Date.ToString("dd.MM.yyyy");
                    DataToBase.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    DataToBase.Photo = ImgFile;
                    DataToBase.Platform = PickerPlatform.SelectedIndex;
                    Connecting.RunInTransaction(() =>
                    {
                        Connecting.Update(DataToBase);
                    });
                }
                else
                {
                    if (Near_photo.Source != null)
                    {
                        Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)Near_photo.Source;
                        ImgFile = objFileImageSource.File;
                    }
                    else
                    {
                        ImgFile = "";
                    }

                    var newData = new Model.SQL.NearMissMy
                    {
                        Violation = ActionIn.Text,
                        ActionTaken = ActionDoIniciator,
                        Status = 1,
                        Job = PickerJob.SelectedIndex,

                        Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),

                        Platform = PickerPlatform.SelectedIndex,
                        Photo = ImgFile
                    };

                    Connecting.Insert(newData);
                }
                var ResultSendToServer = await saveNearMissAndSendToServer.SendToServer();
                if (ResultSendToServer)
                {
                    return true;
                }
            }
            return true;
        }









        /* Закрыть нарушение */
        async void CloseNear(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new MyHazardsList());

        }




    }

}