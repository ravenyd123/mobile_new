﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View.Hazard
{
    public class DateGroup1
    {
        public int Id { get; set; }


        public int Status { get; set; }
        public string Date { get; set; }
        public string StatusName { get; set; }
        public string ImageSource { get; set; }
        public string ActionIn { get; set; }
        public string Iniciator { get; set; }
        public Color StatusColor { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResponsibleHazardList : ToolBar
    {
        public ResponsibleHazardList()
        {
            var DateGroupList = new List<DateGroup1>();
            InitializeComponent();
            this.BackgroundImage = "phone.png";
            Model.SQL.Request request = new Model.SQL.Request();
            Model.GetNameStatus getNameStatus = new Model.GetNameStatus();
            Model.StatusToColor statusToColor = new Model.StatusToColor();
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.Responsible>().Count();
            if (CountBase == 0)
            {
                InfoSW.Text = "Нет нарушений";
            }
            else
            {
                var DataToBase = Connecting.Table<Model.SQL.Responsible>();
                foreach (var s in DataToBase)
                {
                    var soso = s.Status;
                    DateGroupList.Add(new DateGroup1
                    {

                        Id = s.Id,
                        Date = s.Date,
                        ActionIn = s.Violation,
                        StatusName = getNameStatus.GetName(s.Status-1),
                        Iniciator = s.Iniciator,
                        StatusColor = statusToColor.GetColor(s.Status)
                    }) ;

                    MyListView.ItemsSource = DateGroupList;
                }
            }
        }


        /* Тапкаем по нарушению */
        async private void TapGestureRecognizer_Tapped1(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            var dataItem = e.Item as DateGroup1;
            var selectedItem = e.ItemIndex;
            await Navigation.PushAsync(new ResponsibleHazard(dataItem.Id));


            //Удаляем селект
            ((ListView)sender).SelectedItem = null;
        }

        // При нажатии на Добавить
        //async void Add_Hazard1(object sender, EventArgs args)
       // {
       //     int nearId = -1;
        //    await Navigation.PushAsync(new ResponsibleHazard(nearId));

       // }
    }
}