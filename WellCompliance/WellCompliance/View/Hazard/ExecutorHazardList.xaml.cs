﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View.Hazard
{
    public class DateGroup3
    {
        public int Id { get; set; }
        public string StatusName { get; set; }

        public int Status { get; set; }
        public string Date { get; set; }
        public string ImageSource { get; set; }
        public string ActionIn { get; set; }
        public string Iniciator { get; set; }
        public Color StatusColor { get; set; }
        public string Recomendation { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExecutorHazardList : ToolBar
    {
        public ExecutorHazardList()
        {
            var DateGroupList = new List<DateGroup3>();
            InitializeComponent();
            this.BackgroundImage = "phone.png";
            Model.SQL.Request request = new Model.SQL.Request();
            Model.GetNameStatus getNameStatus = new Model.GetNameStatus();
            Model.StatusToColor statusToColor = new Model.StatusToColor();
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.Executor>().Count();
            if (CountBase == 0)
            {
                InfoSW.Text = "Нет нарушений";
            }
            else
            {
                var DataToBase = Connecting.Table<Model.SQL.Executor>();
                foreach (var s in DataToBase)
                {
                    DateGroupList.Add(new DateGroup3
                    {

                        Id = s.Id,
                        Date = s.Date,
                        ActionIn = s.Violation,
                        StatusName = getNameStatus.GetName(s.Status-1),
                        Iniciator = s.Iniciator,
                        StatusColor = statusToColor.GetColor(s.Status),
                        Recomendation = s.Recomendations
                    }); 

                    MyListView.ItemsSource = DateGroupList;
                }
            }
        }


        /* Тапкаем по нарушению */
        async private void TapGestureRecognizer_Tapped1(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            var dataItem = e.Item as DateGroup3;
            var selectedItem = e.ItemIndex;
            await Navigation.PushAsync(new ExecutorHazard(dataItem.Id));


            //Удаляем селект
            ((ListView)sender).SelectedItem = null;
        }

        // При нажатии на Добавить
        //async void Add_Hazard1(object sender, EventArgs args)
        // {
        //     int nearId = -1;
        //    await Navigation.PushAsync(new ResponsibleHazard(nearId));

        // }
    }
}