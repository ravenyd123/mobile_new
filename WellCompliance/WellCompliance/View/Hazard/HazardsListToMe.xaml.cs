﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View.Hazard
{
    public class DateGroupForToMe
    {
        public int Id { get; set; }
        public string Industry { get; set; }
        public string Platform { get; set; }
        public int Status { get; set; }
        public string Date { get; set; }
        public string ImageSource { get; set; }
        public string ActionIn { get; set; }
        public string StatusName { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HazardsListToMe : ToolBar
    {
        public HazardsListToMe()
        {
            var DateGroupList = new List<DateGroupForToMe>();
            InitializeComponent();
            this.BackgroundImage = "phone.png";
            Model.SQL.Request request = new Model.SQL.Request();
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.MeNearmiss>().Count();
            if (CountBase == 0)
            {
                InfoSW.Text = "Нет нарушений";
            }
            else
            {
                var DataToBase = Connecting.Table<Model.SQL.MeNearmiss>();
                foreach (var s in DataToBase)
                {
                    string IcoPath = "ico_nosync2.png";
                    if (s.Status == 1) { IcoPath = "ico_nosync2.png"; }
                    if (s.Status == 2) { IcoPath = "ico_inworkNearMiss.png"; }
                    if (s.Status == 3) { IcoPath = "ico_syncNearMiss"; }
                    if (s.Status == 4) { IcoPath = "ico_ok.png"; }
                    if (s.Status == 5) { IcoPath = "ico_ok.png"; }
                    if (s.Status == 6) { IcoPath = "ico_ok.png"; }
                    if (s.Status == 7) { IcoPath = "ico_ok.png"; }
                    var StatusName = Connecting.Get<Model.SQL.NearMissStatus>(s.Status);
                    DateGroupList.Add(new DateGroupForToMe
                    {
                        Platform = s.Platform.ToString(),
                        ImageSource = IcoPath,
                        Id = s.Id,
                        Date = s.Date,
                        ActionIn = s.ActionTaken,
                        StatusName = StatusName.NameStatus
                    });
                }
                MyListView.ItemsSource = DateGroupList;
            }
        }


        /* Тапкаем по нарушению */
        async private void TapGestureRecognizer_Tapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            var dataItem = e.Item as DateGroup;
            var selectedItem = e.ItemIndex;
            await Navigation.PushAsync(new HazardToMe(dataItem.Id));


            //Удаляем селект
            ((ListView)sender).SelectedItem = null;
        }

        // При нажатии на Добавить
        async void Add_Hazard(object sender, EventArgs args)
        {
            var nearId = -1;
            await Navigation.PushAsync(new HazardToMe(nearId));

        }
    }
}