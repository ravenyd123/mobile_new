﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace WellCompliance.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : ContentPage
    {
       // Model.Network network = new Model.Network();
        Model.SQL.Request request = new Model.SQL.Request();
        Model.Extensive extensive = new Model.Extensive();
        ViewModel.Notification notification = new ViewModel.Notification();
        ViewModel.Indicator indicator = new ViewModel.Indicator();

        public Settings()
        {
            var scroll = new ScrollView();
            NavigationPage.SetHasBackButton(this, false); // Убираем стрелку назад
            InitializeComponent();

            /* Отоброжаем данные настроек */
            var Connecting = request.BD();
            var DataToBase = Connecting.Get<Model.SQL.Settings>(1);
            Server.Text = DataToBase.Url;
            Protocol.SelectedItem = DataToBase.Protocol;
            Port.Text = DataToBase.Port;
            HazardMy.Text = DataToBase.HazardMy;
            HazardToMe.Text = DataToBase.HazardToMe;
            Report1.Text = DataToBase.Report1;
            Report2.Text = DataToBase.Report2;
            Report3.Text = DataToBase.Report3;
            Report4.Text = DataToBase.Report4;
        }

        /* Действия при сохранении */
        public async void SaveSettings(object sender, EventArgs args)
        {
            if (Server.Text == "")
            {
                notification.DisplayMessage(
                    "Настройки",
                    "Укажите адрес сервера",
                    "OK");
            }
            else
            {
                SaveSet.IsEnabled = false;

                /* индикатор */
                var control = indicator.Icon("Проверка сервера");
                grid.Children.Add(control);

                string NomberPort;
                var Data = new NameValueCollection();
                Data.Add("action", "chekServer");
                if (Port.Text == "")
                {
                    NomberPort = "80";
                }
                else
                {
                    NomberPort = Port.Text;
                };
                string Url = Protocol.SelectedItem + Server.Text + ":" + NomberPort + "/app/views/nearmiss/mobile.php";
                string StatusDataTransfer = await extensive.Datatransfer(Url, Data);
                if (StatusDataTransfer == "")
                {
                    grid.Children.Remove(control); // Выключаем индикатор
                    notification.DisplayMessage(
                        "Настройки",
                        "Сервер не отвечает. Проверьте правильность заполненных данных",
                        "OK");
                   
                   
                    SaveSet.IsEnabled = true;
                }
                else
                {
                    var kod = await extensive.GetHashStringInMD5(await extensive.GetHashStringInMD5(Protocol.SelectedItem + Server.Text));
                    if (StatusDataTransfer == kod)
                    {
                        bool sts;
                        var Connecting = request.BD();
                        var DataToBase = Connecting.Get<Model.SQL.Settings>(1);
                        if (DataToBase.Url == "")
                        {
                            sts = false;
                        }
                        else
                        {
                            sts = true;
                        }
                        DataToBase.Url = Server.Text;
                        DataToBase.Port = Port.Text;
                        DataToBase.Protocol = Protocol.SelectedItem.ToString();
                        DataToBase.HazardMy = HazardMy.Text;
                        DataToBase.HazardToMe = HazardToMe.Text;
                        DataToBase.Report1 = Report1.Text;
                        DataToBase.Report2 = Report2.Text;
                        DataToBase.Report3 = Report3.Text;
                        DataToBase.Report4 = Report4.Text;
                        Connecting.RunInTransaction(() =>
                        {
                            Connecting.Update(DataToBase);
                        });
                        if (sts) notification.DisplayMessage("Сохранение настроек", "Для сохранения настроек необходимо перезайти в программу. Все данные, которые не были отправлены на сервер будут удалены", "OK");
                        /* индикатор */
                        control = indicator.Icon("Удаление старых данных");
                        grid.Children.Add(control);
                        bool status = await extensive.ClearProfileData();
                        if (status)
                        {
                            grid.Children.Remove(control); // Выключаем индикатор
                            await Navigation.PushAsync(new Authorization());
                        } else
                        {
                            notification.DisplayMessage("Изменение настроек", "Неожиданная ошибка в работе с базой данных", "OK");
                        }
                    }
                    else
                    {
                        notification.DisplayMessage(
                            "Настройки",
                            "Неверный ответ сервера. Проверьте правильность заполненных данных",
                            "OK");
                        grid.Children.Remove(control); // Выключаем индикатор
                        SaveSet.IsEnabled = true;
                    }
                }
            }
        }

        /* Событие при смене протокола http / https */
        void ChangeProtocol(object sender, EventArgs e)
        {
            if (Protocol.SelectedItem == "https://")
            {
                Port.Text = "443";
            }
            else 
            {
                Port.Text = "80";
            }
        }

        /* Закрытие настроек */
        public async void CloseSettings(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
    }
}