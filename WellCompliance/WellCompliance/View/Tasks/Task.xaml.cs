﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WellCompliance.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WellCompliance.View.Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Task : ToolBar
    {
        ViewModel.Indicator indicator = new ViewModel.Indicator();
        Model.SQL.Request request = new Model.SQL.Request();
        public Task(int taskid)
        {
            var Connecting = request.BD();
            InitializeComponent();
            if (taskid != -1)
            {
                var DataToBase = Connecting.Get<Model.SQL.Tasks>(taskid); 
                Title = "Задача №" + DataToBase.Id;
                NameTask.Text = DataToBase.Name;
                DescriptionTask.Text = DataToBase.Description;
                PickerPlatform.SelectedItem = DataToBase.Platform.ToString();
                CreateDate.Text = DataToBase.Date;
                DeadlineDate.Text = DataToBase.Deadline;
            }
        }

        /* СОхранить Задачу */
        async void SaveTask(object sender, EventArgs args)
        {
           // await Navigation.PushAsync(new View.Tasks.TasksList());
        }

        /* Закрыть Задачу */
        async void CloseTask(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new View.Tasks.TasksList());
        }

       
    }
}