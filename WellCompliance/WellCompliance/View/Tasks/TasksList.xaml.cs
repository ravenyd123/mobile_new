﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View.Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class DateGroup
    {
        public int IdTask { get; set; }
        public string Platform { get; set; }
        public string Status { get; set; }
        public string DateCreate { get; set; }
        public string Deadline { get; set; }
        public string Finished { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Info { get; set; }
    }

    public partial class TasksList : ContentPage
    {
        public TasksList()
        {
            InitializeComponent();
            var DateGroupList = new List<DateGroup>();
            this.BackgroundImage = "phone.png";
            Model.SQL.Request request = new Model.SQL.Request();
            var Connecting = request.BD();
            var CountBase = Connecting.Table<Model.SQL.Tasks>().Count();
            if (CountBase == 0)
            {
                Inf.Text = "Нет задач";
            }
            else
            {
                var DataToBase = Connecting.Table<Model.SQL.Tasks>();
                foreach (var s in DataToBase)
                {
                    var a = s.Status;
                    string IcoPath = "ico_nosync2.png";
                    //  if (s.Status == 1) { IcoPath = "ico_nosync2.png"; }
                    //   if (s.Status == 2) { IcoPath = "ico_inworkNearMiss.png"; }
                    //   if (s.Status == 3) { IcoPath = "ico_syncNearMiss"; }
                    //    if (s.Status == 4) { IcoPath = "ico_ok.png"; }
                    //   if (s.Status == 5) { IcoPath = "ico_ok.png"; }
                    //   if (s.Status == 6) { IcoPath = "ico_ok.png"; }
                    //   if (s.Status == 7) { IcoPath = "ico_ok.png"; }
                    //   var StatusName = Connecting.Get<Model.SQL.NearMissStatus>(s.Status);
                    var idPlatform = Connecting.Query<Model.SQL.Platforms>("SELECT `id` FROM `Platforms` WHERE `Serverid`=" + s.Platform + "");
                    foreach (var t in idPlatform) { 
                        var PlatformName = Connecting.Get<Model.SQL.Platforms>(t.Id);
                        DateGroupList.Add(new DateGroup
                        {
                            //  ImageSource = IcoPath,
                            Info = "Задача №" + s.Id + " от " + s.Date + " '"+ s.Name + "' ",
                           
                            Platform = PlatformName.PlatformsName,
                            Status = s.Status,
                        }) ;
                    }
                }
                MyListView.ItemsSource = DateGroupList;
            }
        }
        /* Тапкаем по задаче */
        async private void TapGestureRecognizer_Tapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            var dataItem = e.Item as DateGroup;
            var selectedItem = e.ItemIndex;
            await Navigation.PushAsync(new Task(dataItem.IdTask));
            //Удаляем селект
            ((ListView)sender).SelectedItem = null;
        }

        async void Add_Hazard(object sender, EventArgs args)
        {
            ((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new Task(-1));

        }
    }
}