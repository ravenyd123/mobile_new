﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;
using WellCompliance.Model;

namespace WellCompliance.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainMenuPage : ToolBar
    {
        Model.SQL.Request request = new Model.SQL.Request();
        ViewModel.Notification notification = new Notification();
		public MainMenuPage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasBackButton(this, false); // Убираем стрелку НАЗАД из toolbars

            /* Загружаем и заполняем название проверок */
            var connecting = request.BD();
            var DataToBase = connecting.Get<Model.SQL.Settings>(1);
            Responsible.Text = DataToBase.HazardToMe;
            HazardMy.Text = DataToBase.HazardMy;
            Report1.Text = DataToBase.Report1;
            Report2.Text = DataToBase.Report2;
            Report3.Text = DataToBase.Report3;
            Report4.Text = DataToBase.Report4;
            var DataToBase2 = connecting.Get<Model.SQL.Profile>(1);


            if (DataToBase2.Role == "Iniciator")
            {
                Executor.IsVisible = false;
                Responsible.IsVisible = false;
                Report1.IsVisible = false;
                Report2.IsVisible = false;
                Report3.IsVisible = false;
                Report4.IsVisible = false;
            }
            else if (DataToBase2.Role == "Responsible")
            {
                Executor.IsVisible = false;
                Report1.IsVisible = false;
                Report2.IsVisible = false;
                Report3.IsVisible = false;
                Report4.IsVisible = false;
            }
            else if (DataToBase2.Role == "Executor")
            {
                
                Responsible.IsVisible = false;
                Report1.IsVisible = false;
                Report2.IsVisible = false;
                Report3.IsVisible = false;
                Report4.IsVisible = false;
            }
            else if (DataToBase2.Role == "Manager")
            {

                Report1.IsVisible = false;
                Report2.IsVisible = false;
                Report3.IsVisible = false;
                Report4.IsVisible = false;
            }
            else {

                Executor.IsVisible = false;
                Responsible.IsVisible = false;
                Report1.IsVisible = false;
                Report2.IsVisible = false;
                Report3.IsVisible = false;
                Report4.IsVisible = false;

            }



        }

        /* Обработчик кнопки Неамисс мне */
        public async void ClickedExecutor(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Hazard.ExecutorHazardList());
        }
        /* Обработчик кнопки Неамисс мой */
        public async void ClickedHazardMy(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Hazard.IniciatorHazardList());
        }
        public async void ClickedResponsible(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Hazard.ResponsibleHazardList());
        }


        public async void ClickedMyTasks(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Tasks.TasksList());
        }
        public async void ClickedReport1(object sender, EventArgs e)
        {
           // await Navigation.PushAsync(new Reports.ChekList.Group(1));
        }
        public async void ClickedReport2(object sender, EventArgs e)
        {
          //  await Navigation.PushAsync(new Reports.ChekList.Group(2));
        }
        public async void ClickedReport3(object sender, EventArgs e)
        {
          //  await Navigation.PushAsync(new Reports.ChekList.Group(3));
        }
        public async void ClickedReport4(object sender, EventArgs e)
        {
           // await Navigation.PushAsync(new Reports.ChekList.Group(4));
        }


        /* Интерфейс для закрытия приложения при наждатии на назад */
                /* Сама ф-ция которая срабатывает при нажатии назад */
        protected override bool OnBackButtonPressed()
        {
            INativeHelper nativeHelper = null;
            nativeHelper = DependencyService.Get<INativeHelper>();
            if (nativeHelper != null)
            {
                nativeHelper.CloseApp();
            }

            return base.OnBackButtonPressed();
        }

    }
}