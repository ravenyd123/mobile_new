﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Start : ToolBarSmall
	{
        Model.SQL.Request request = new Model.SQL.Request();
        ViewModel.Notification notification = new ViewModel.Notification();
        Model.Prepare prepare = new Model.Prepare();

		public Start ()
		{
            NavigationPage.SetHasBackButton(this, false); // Убираем стрелку назад
            if (request.ChekExistBD())
            {
                if (CHekExistsRecordToSettings())
                {
                    GoAuthorization();
                } else {
                    if (prepare.PrepareTables())
                    {
                        GoSettings();
                    }
                    else
                    {
                        notification.DisplayMessage("Ошибка!", "Невозможно создать таблицы в базе. Возможно нехватает свободного места или повреждена память", "OK");
                    }
                }
            } else {
                if (request.CreateBD())
                {
                    if (prepare.PrepareTables())
                    {
                        GoSettings();
                    } else
                    {
                        notification.DisplayMessage("Ошибка!", "Невозможно создать таблицы в базе. Возможно нехватает свободного места или повреждена память", "OK");
                    }
                }
                else {
                    notification.DisplayMessage("Ошибка!", "Невозможно создать файл базы. Возможно нехватает свободного места или повреждена память", "OK");
                }
            }
        }

        public bool CHekExistsRecordToSettings()
        {
            try
            {
                var Connecting = request.BD();
                var DataToBase = Connecting.Get<Model.SQL.Settings>(1);
                if (DataToBase.Url == "")
                {
                    return false;
                } else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async void GoSettings()
        {
            Title = "Настройки";
            await Navigation.PushAsync(new Settings());
        }

        public void GoAuthorization()
        {
            Title = "Авторизация";
            Navigation.PushAsync(new Authorization());
        }
    }
}