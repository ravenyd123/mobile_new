﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WellCompliance.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Photo : ContentPage
    {
        public Photo(string ImgFile)
        {
            var scroll = new ScrollView();
            Content = scroll;
            InitializeComponent();
            ImagesFull.Source = ImgFile;
            var tapImages = new TapGestureRecognizer();

            tapImages.Tapped += async (s, e) =>
            {
                await Navigation.PopAsync();

            };
            ImagesFull.GestureRecognizers.Add(tapImages);
        }
    }
}