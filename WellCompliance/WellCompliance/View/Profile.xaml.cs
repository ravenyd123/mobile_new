﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;

namespace WellCompliance.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
   
    public partial class Profile : ToolBar
	{
        Model.SQL.Request request = new Model.SQL.Request();
        public Profile ()
		{
			InitializeComponent ();
            var connecting = request.BD();
            var DataToBaseProfile = connecting.Get<Model.SQL.Profile>(1);
            Famyli.Text = DataToBaseProfile.Surname;
            Name.Text = DataToBaseProfile.Name;
            FatherName.Text = DataToBaseProfile.FatherName;
            Email.Text = DataToBaseProfile.Email;
        }

        public async void CloseButton(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}