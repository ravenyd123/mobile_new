﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace WellCompliance.Model
{
    class SendNearMiss
    {

        GetMyId myid = new GetMyId();
        Model.Network network = new Model.Network();
        Model.SQL.Request request = new Model.SQL.Request();

        /* Получение новых и измененных Tasks */
        public async Task<bool> SendNM()
        {
            try
            {
                var Connecting = request.BD();
                var userid = myid.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "GetNearmiss");
                Data.Add("userid", userid);
                var AnswerGetTasks = await network.DataTransfer(Url, Data);

                if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                    var data = JsonConvert.DeserializeObject<List<JsonTasks>>(AnswerGetTasks); // Распаковка json
                    foreach (var s in data)
                    {
                        var Quantity = Connecting.Query<SQL.Tasks>("SELECT `Id` FROM `Tasks` WHERE `Id`=" + s.id + " LIMIT 1");
                        if (Quantity.Count == 0)
                        {
                            var newData = new SQL.Tasks
                            {
                                Id = s.id,
                                Platform = s.platform,
                                Status = s.status,
                                Date = s.date,
                                Deadline = s.deadline,
                                Finished = s.finished,
                                Name = s.name,
                                Description = s.description,
                                Director = s.director,
                                Responsible = s.responsible
                            };
                            Connecting.Insert(newData);
                        }
                        else
                        {
                            var TsksToBase = Connecting.Get<SQL.Tasks>(s.id);
                            TsksToBase.Platform = s.platform;
                            TsksToBase.Status = s.status;
                            TsksToBase.Date = s.date;
                            TsksToBase.Deadline = s.deadline;
                            TsksToBase.Finished = s.finished;
                            TsksToBase.Name = s.name;
                            TsksToBase.Description = s.description;
                            TsksToBase.Director = s.director;
                            TsksToBase.Responsible = s.responsible;
                            Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(TsksToBase);
                            });
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
