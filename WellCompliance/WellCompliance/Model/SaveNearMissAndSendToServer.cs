﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace WellCompliance.Model
{
    class SaveNearMissAndSendToServer
    {
        /* JSON формат при отправке Near Miss при синхронизации */
        [DataContract]
        public class Datanearmiss
        {
            [DataMember]
            public int Userid { get; set; }
            [DataMember]
            public int Id { get; set; }
            [DataMember]
            public int Job { get; set; }
            [DataMember]
            public string ActionIn { get; set; }
            [DataMember]
            public string ActionDo { get; set; }
            [DataMember]
            public string Geolocal { get; set; }
            [DataMember]
            public string CreateDate { get; set; }
            [DataMember]
            public int Platform { get; set; }
            [DataMember]
            public string Img { get; set; }
        }

        /* JSON формат при отправке Near Miss при синхронизации */
        [DataContract]
        public class Datanearmiss2
        {
            [DataMember]
            public int Userid { get; set; }
            [DataMember]
            public int Id { get; set; }
            [DataMember]
            public string ActionIn { get; set; }
            [DataMember]
            public string ActionDo { get; set; }
            [DataMember]
            public int Platform { get; set; }
            [DataMember]
            public string Img { get; set; }
        }

        /* JSON формат при отправке Near Miss при синхронизации */
        [DataContract]
        public class Datanearmiss3
        {
            [DataMember]
            public int Id { get; set; }
            [DataMember]
            public string DeadlineDate { get; set; }
            [DataMember]
            public int Executor { get; set; }
            [DataMember]
            public string Recomendation { get; set; }
            [DataMember]
            public string ReasonOfInconsistency { get; set; }
        }

        /* JSON формат при отправке Near Miss ответственного при синхронизации */
        [DataContract]
        public class DatanearmissResp
        {
            [DataMember]
            public int Userid { get; set; }
            [DataMember]
            public int Id { get; set; }
            [DataMember]
            public string ActionIn { get; set; }
            [DataMember]
            public string ActionDo { get; set; }
            [DataMember]
            public int Platform { get; set; }
            [DataMember]
            public string Img { get; set; }
        }

        /* JSON ответа при синхронизации */
        public class DataResult
        {
            public int Id { get; set; }
            public int Status { get; set; }
            public int idserv { get; set; }
        }

        Model.SQL.Request request = new Model.SQL.Request();
        Model.ConvertBase64 convertBase64 = new ConvertBase64();
        Model.Network network = new Model.Network();
        GetMyId myid = new GetMyId();
        string json;
        public async Task<bool> SendToServer ()
        {
                var Connecting = request.BD();
                var DataToBase = Connecting.Query<SQL.NearMissMy>("SELECT * FROM `NearMissMy` WHERE `id`=(SELECT MAX(id) FROM `NearMissMy`)");
                var datanearmiss = new List<Datanearmiss>();
                foreach (var s in DataToBase)
                {
                var IdJob = Connecting.Get<SQL.Jobs>(s.Job);
                var IdPlatform = Connecting.Get<SQL.Platforms>(s.Platform);

                string dataimg;
                    if (s.Photo != "")
                    {
                        dataimg = convertBase64.ConvertImageToBase64(s.Photo);
                    }
                    else
                    {
                        dataimg = "";
                    }
                    var userID = Convert.ToInt32(myid.getMyId());
                    
                    datanearmiss.Add(new Datanearmiss()
                    {
                        Userid = userID,
                        Id = s.Id,
                        Job = IdJob.Serverid+1,
                        ActionDo = s.ActionTaken,
                        ActionIn = s.Violation,
                        Platform = IdPlatform.Serverid,
                        Geolocal = s.Geolocation,
                        CreateDate = s.Date,
                        Img = dataimg
                    });
                    /* Если данные есть, то их отправляем */
                    if (datanearmiss != null)
                    {
                        json = JsonConvert.SerializeObject(datanearmiss);
                        var SendDataNearMissJson = network.SaveNMAndSendToServer(json);
                        var ResultNearMiss = JsonConvert.DeserializeObject<DataResult[]>(SendDataNearMissJson);
                        foreach (var a in ResultNearMiss)
                        {
                        await UpdateStatus(a.Id, a.Status, a.idserv);
                        }
                    }
                
                }
            return true;
        }

        public async Task<bool> SendToServer2(int nearId)
        {
            var Connecting = request.BD();
            string query;
            if (nearId == -1)
            {
               query = "SELECT * FROM `Iniciator` WHERE `id`=(SELECT MAX(id) FROM `Iniciator`)";
            } else
            {
                query = "SELECT * FROM `Iniciator` WHERE `id`=" + nearId;
            }
             
             var DataToBase = Connecting.Query<SQL.Iniciator>(query);
            
            var datanearmiss = new List<Datanearmiss2>();
            foreach (var s in DataToBase)
            {
                string dataimg;
                if (s.Photo != "")
                {
                    dataimg = convertBase64.ConvertImageToBase64(s.Photo);
                }
                else
                {
                    dataimg = "";
                }
                int pl = s.Platform + 1;
                var userID = Convert.ToInt32(myid.getMyId());
                var idPlatform = Connecting.Query<SQL.Platforms>("SELECT `Serverid` FROM `Platforms` WHERE `Id`=" + pl);
                foreach (var b in idPlatform)
                {
                    datanearmiss.Add(new Datanearmiss2()
                    {
                        Userid = userID,
                        Id = s.Id,
                        ActionDo = s.ActionTaken,
                        ActionIn = s.Violation,
                        Platform = b.Serverid,
                        Img = dataimg
                    });
                }
                /* Если данные есть, то их отправляем */
                if (datanearmiss != null)
                {
                    json = JsonConvert.SerializeObject(datanearmiss);
                    var SendDataNearMissJson = network.SaveNMAndSendToServer2(json);
                    var ResultNearMiss = JsonConvert.DeserializeObject<DataResult[]>(SendDataNearMissJson);
                    foreach (var a in ResultNearMiss)
                    {
                        await UpdateStatus(a.Id, a.Status, a.idserv);
                    }
                }

            }
            return true;
        }

        /* Обновляем в мобилке статусы иайди тех NM которые были удачно отправлены на сервер */
        public async Task<bool> UpdateStatus(int Id, int Status, int idserv)
        {
            try
            {
                var Connecting = request.BD();
                var DataToBase = Connecting.Get<SQL.Iniciator>(Id);
                DataToBase.Status = Status;
                DataToBase.IdServ = idserv;
                Connecting.RunInTransaction(() =>
                {
                    Connecting.Update(DataToBase);
                });
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }


        public async Task<bool> SendToServer3(int nearId)
        {
            var Connecting = request.BD();
            string query;

                query = "SELECT * FROM `Responsible` WHERE `id`=" + nearId;

            var DataToBase = Connecting.Query<SQL.Responsible>(query);

            var datanearmiss = new List<Datanearmiss3>();
            foreach (var s in DataToBase)
            {
                    datanearmiss.Add(new Datanearmiss3()
                    {
                        Id = s.Id,
                        DeadlineDate = s.DeadlineDate,
                        Executor = s.Executor,
                        Recomendation = s.Recomendation,
                        ReasonOfInconsistency = s.ReasonOfInconsistency
                    });
               
                /* Если данные есть, то их отправляем */
                if (datanearmiss != null)
                {
                    json = JsonConvert.SerializeObject(datanearmiss);
                    var SendDataNearMissJson = network.SaveNMAndSendToServer3(json);
    
                }

            }
            return true;
        }



    }
}
