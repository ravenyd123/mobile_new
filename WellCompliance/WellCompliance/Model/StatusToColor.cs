﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace WellCompliance.Model
{
    class StatusToColor
    {
        public Color GetColor(int status)
        {
            Color color;
            color = Color.Black;
            if (status == 1) color = Color.LightGreen; // На отправке
            if (status == 2) color = Color.Green; // Отправлен
            if (status == 3) color = Color.DarkGreen; // Просмотрен
            if (status == 4) color = Color.Blue; // В работе
            if (status == 5) color = Color.Yellow; // На согласовании
            if (status == 6) color = Color.DarkBlue;// выполнен
            if (status == 7) color = Color.Black;
            if (status == 8) color = Color.Red;
            if (status == 9) color = Color.Gray;
            if (status == 10) color = Color.Violet;
            return color;
        }
        
    }
}
