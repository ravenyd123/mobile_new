﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Specialized;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WellCompliance.ViewModel;
using System.Runtime.CompilerServices;

namespace WellCompliance.Model
{

    /* JSON получение nearmiss */
    public class JsonNewNMForExecutor
    {
        public int id { get; set; }
        public int status { get; set; }
        public string actionIn { get; set; }
        public string actionDo { get; set; }
        public string iniciator_fio { get; set; }
        public string responsible_fio { get; set; }
        public string platform_name { get; set; }
        public string createDate { get; set; }
        public string deadlineDate { get; set; }
        public string demand { get; set; }
        public string photo { get; set; }
    }

    /* JSON получение JsonNewNM */
    public class JsonNewNM
    {
        public int id { get; set; }
        public int status { get; set; }
        public string actionIn { get; set; }
        public string actionDo { get; set; }
        public string iniciator_fio { get; set; }
        public int id_responsible { get; set; }
        public string platform_name { get; set; }
        public string createDate { get; set; }
        public string deadlineDate { get; set; }
        public string demand { get; set; }
        public string photo { get; set; }
    }


    /* JSON получение статусы и пуши */
    public class JsonPushAndData
    {
        public int id { get; set; }
        public int id_tel { get; set; }
        public int status { get; set; }
        public string push { get; set; }
        public string type { get; set; }
        public string wasdone_executor { get; set; }
        public string ReasonOfInconsistency { get; set; }
    }




    /* JSON получение Tasks */
    public class JsonTasks
    {
        public int id { get; set; }
        public int platform { get; set; }
        public string status { get; set; }
        public string date { get; set; }
        public string deadline { get; set; }
        public string finished { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int director { get; set; }
        public int responsible { get; set; }
    }

    class SendTasks
    {
        GetMyId myid = new GetMyId();
        Model.Network network = new Model.Network();
        Model.SQL.Request request = new Model.SQL.Request();
        Model.Base64ToImagesFile base64ToImagesFile = new Base64ToImagesFile();

       int id;

        /* Получение новых и измененных Tasks */
        public async Task<bool> GetTasks()
        {
            try
            {
                var Connecting = request.BD();
                var userid = myid.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "GetTasks");
                Data.Add("userid", userid);
                var AnswerGetTasks = await network.DataTransfer(Url, Data);
                if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                    var data = JsonConvert.DeserializeObject<List<JsonTasks>>(AnswerGetTasks); // Распаковка json
                    foreach (var s in data)
                    {
                        var Quantity = Connecting.Query<SQL.Tasks>("SELECT `Id` FROM `Tasks` WHERE `Id`=" + s.id + " LIMIT 1");
                        if (Quantity.Count == 0)
                        {
                            var newData = new SQL.Tasks
                            {
                                Id = s.id,
                                Platform = s.platform,
                                Status = s.status,
                                Date = s.date,
                                Deadline = s.deadline,
                                Finished = s.finished,
                                Name = s.name,
                                Description = s.description,
                                Director = s.director,
                                Responsible = s.responsible
                            };
                            Connecting.Insert(newData);
                        }
                        else
                        {
                            var TsksToBase = Connecting.Get<SQL.Tasks>(s.id);
                            TsksToBase.Platform = s.platform;
                            TsksToBase.Status = s.status;
                            TsksToBase.Date = s.date;
                            TsksToBase.Deadline = s.deadline;
                            TsksToBase.Finished = s.finished;
                            TsksToBase.Name = s.name;
                            TsksToBase.Description = s.description;
                            TsksToBase.Director = s.director;
                            TsksToBase.Responsible = s.responsible;
                            Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(TsksToBase);
                            });
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        /* Проверка на новый nm */
        public async Task<bool> NewNM()
        {
            try
            {
                var Connecting = request.BD();
                var userid = myid.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "NewNM");
                //Data.Add("userid", userid);
                var AnswerGetTasks = await network.DataTransfer(Url, Data);

                if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                    var data = JsonConvert.DeserializeObject<List<JsonNewNM>>(AnswerGetTasks); // Распаковка json
                    foreach (var s in data)
                    {
                        id = s.id;

                        var idExecutor = Connecting.Query<Model.SQL.ExecuterList>("SELECT `id` FROM `ExecuterList` WHERE `ServerId`=" + s.id_responsible + "");
                        foreach (var b in idExecutor) {

                        }

                        var newData = new SQL.Responsible
                            {
                            Id = s.id,
                            Status = s.status+1,
                            Violation = s.actionIn,
                            ActionTaken = s.actionDo,
                            Iniciator = s.iniciator_fio,
                            Platform = s.platform_name,
                            Date = s.createDate,
                            DeadlineDate = s.deadlineDate,
                            Recomendation = s.demand,
                            Executor = s.id_responsible,
                            Photo = base64ToImagesFile.GetImageFile(s.photo)
                            };
                            Connecting.Insert(newData);
                    }
                    LocalNotifications localNotifications = new LocalNotifications();
                    string title = $"NearMiss";
                    string message = $"Вы назначены ответственным за устранение Near Miss №" + id;
                    string type = "Responsible";
                    localNotifications.CreateNotification(title, message, type, id);
                    return true;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Проверка на новый nm для исполнителя*/
        public async Task<bool> NewNMForExecutor()
        {
            try
            {
                var Connecting = request.BD();
                var userid = myid.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "NewNMForExecutor");
             //   Data.Add("userid", userid);
               var AnswerGetTasks = await network.DataTransfer(Url, Data);
                if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                    var data = JsonConvert.DeserializeObject<List<JsonNewNMForExecutor>>(AnswerGetTasks); // Распаковка json
                    foreach (var s in data)
                    {
                        id = s.id;
                        var newData = new SQL.Executor
                        {
                            Id = s.id,
                            Status = s.status+1,
                            Violation = s.actionIn,
                            ActionTaken = s.actionDo,
                            Iniciator = s.iniciator_fio,
                            Responsible = s.responsible_fio,
                            Platform = s.platform_name,
                            Date = s.createDate,
                            Recomendations = s.demand,
                            DeadlineDate = s.deadlineDate,
                            Photo = base64ToImagesFile.GetImageFile(s.photo)
                        };
                        Connecting.Insert(newData);
                    }
                    LocalNotifications localNotifications = new LocalNotifications();
                    string title = $"NearMiss";
                    string message = $"Вы назначены исполнителем за устранение Near Miss №" + id;
                    string type = "Executor";
                    localNotifications.CreateNotification(title, message, type, id);
                    return true;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Проверка на новые статусы и пуши*/
        public async Task<bool> NewStatusAndPush()
        {
              try
               {
            Console.WriteLine("Запуск1");
            var Connecting = request.BD();
                var userid = myid.getMyId();
                string role = myid.getMyRole();
                var Data = new NameValueCollection();
           
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "PushAndData");
            //    Data.Add("userid", userid);
            Console.WriteLine("Запуск2");
            var AnswerGetTasks = await network.DataTransfer(Url, Data);
            Console.WriteLine("Запуск3");
            if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                Console.WriteLine("Запуск4");
                var data = JsonConvert.DeserializeObject<List<JsonPushAndData>>(AnswerGetTasks); // Распаковка json
                    foreach (var s in data)
                    {
                        if (s.type == "iniciator")
                        {
                        Console.WriteLine("Запуск5");
                        var ToBase2 = Connecting.Get<SQL.Iniciator>(s.id_tel);
                        Console.WriteLine("1");
                        ToBase2.Status = s.status+1;
                        Console.WriteLine("2");
                        Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(ToBase2);
                            });
                        Console.WriteLine("3");
                        LocalNotifications localNotifications = new LocalNotifications();
                        Console.WriteLine("4");
                        string title = $"NearMiss";
                        Console.WriteLine("5");
                        string message = s.push;
                        Console.WriteLine("6");
                        string type = "Iniciator_status";
                        Console.WriteLine("Запуск6 титл: " + title + ";месс: " + message + ";тип: " + type + ";айдителефона: " + s.id_tel);
                        Console.WriteLine("7");
                        localNotifications.CreateNotification(title, message, type, s.id_tel);
                        Console.WriteLine("8");
                        Console.WriteLine("Запуск7");
                    }

                        if (s.type == "responsible")
                        {
                           var ToBase = Connecting.Get<SQL.Responsible>(s.id);
                            ToBase.Status = s.status+1;
                            ToBase.WhatDo = s.wasdone_executor;
                            Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(ToBase);
                            });
                            LocalNotifications localNotifications = new LocalNotifications();
                            string title = $"NearMiss";
                            string message = s.push;
                            string type = "Responsible_status";
                            localNotifications.CreateNotification(title, message, type, s.id);
                        }

                        if (s.type == "executor")
                        {
                            var ToBase = Connecting.Get<SQL.Executor>(s.id);
                            ToBase.Status = s.status+1;
                            ToBase.ReasonOfInconsistency = s.ReasonOfInconsistency;

                            Connecting.RunInTransaction(() =>
                            {
                                Connecting.Update(ToBase);
                            });

                            LocalNotifications localNotifications = new LocalNotifications();
                            string title = $"NearMiss";
                            string message = s.push;
                            string type = "Executor_status";
                            localNotifications.CreateNotification(title, message, type, s.id);
                        }
                    }
                    return true;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
           }
        }
    }
}
