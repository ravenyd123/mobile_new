﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WellCompliance.Model
{
    class RandomKey
    {
        public string GenerateKey()
        {
            var rnd = new Random();
            var s = new StringBuilder();

            for (int i = 0; i < 14; i++)
                s.Append((char)rnd.Next('a', 'z'));

            var str = s.ToString();
            return str;
        }
    }
}
