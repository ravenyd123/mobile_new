﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace WellCompliance.Model
{
    /* JSON Профиль пользователя */
    public class UserAccount
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Father_name { get; set; }
        public string Email { get; set; }
        public string Platform { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public string Rols { get; set; }
        public string Token { get; set; }
    }

    /* JSON Виды работ */
    public class JobsType
    {
        public int Id { get; set; }
        public string JobsName { get; set; }
    }

    /* JSON платформы */
    public class Platforms
    {
        public int Id { get; set; }
        public string PlatformName { get; set; }
    }

    /* JSON ActionDo - действия инициатора */
    public class ActionDo
    {
        public int Id { get; set; }
        public string ActionDO { get; set; }
    }

    /* JSON статусы Near Miss */
    public class statusNearMiss
    {
        public int Id { get; set; }
        public string status { get; set; }
    }


    /* JSON список Исполнителей */
    public class listExecutors
    {
        public int Id { get; set; }
        public string fio { get; set; }
    }

    /* JSON получение моих Near Miss */
    public class DloadNearMissMy
    {
        public int id { get; set; }
        public int id_tel { get; set; }
        public int job { get; set; }
        public string actionIn { get; set; }
        public string actionDo { get; set; }
        public int status { get; set; }
        public string createDate { get; set; }
        public string photo { get; set; }
        public string dateValidation { get; set; }
        public int platform { get; set; }
    }

    /* JSON получение Executor */
    public class DloadExecutor
    {
        public int id { get; set; }
        public int status { get; set; }
        public string actionIn { get; set; }
        public string actionDo { get; set; }
        public string iniciator_fio { get; set; }
        public string responsible_fio { get; set; }
        public string platform_name { get; set; }
        public string createDate { get; set; }
        public string deadlineDate { get; set; }
        public string demand { get; set; }
        public string photo { get; set; }
    }

    /* JSON получение у ответственного */
    public class DloadResponsible
    {
        public int id { get; set; }
        public int status { get; set; }
        public string actionIn { get; set; }
        public string actionDo { get; set; }
        public string iniciator_fio { get; set; }
        public string platform_name { get; set; }
        public string createDate { get; set; }
        public string deadlineDate { get; set; }
        public string demand { get; set; }
        public int id_responsible { get; set; }
        public string photo { get; set; }

    }

    /* JSON получение Near Miss, адресованных мне */
    public class DloadNearMissToMe
    {
        public int id { get; set; }
        public int job { get; set; }
        public string iniciator { get; set; }
        public string violation { get; set; }
        public string actionTaken { get; set; }
        public int status { get; set; }
        public string createDate { get; set; }
        public string planDate { get; set; }
        //public string photo { get; set; }
        public string dateValidation { get; set; }
        public int platform { get; set; }
        public string necessaryActivities { get; set; }
    }

    /* JSON получение Tasks */
    public class DloadTasks
    {
        public int id { get; set; }
        public int platform { get; set; }
        public string status { get; set; }
        public string date { get; set; }
        public string deadline { get; set; }
        public string finished { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int director { get; set; }
        public int responsible { get; set; }
    }

        /* Получаем время и дату сейчас */
        public static class DateTimeExtensions
    {
        public static long MillisecondsTimestamp(this DateTime date)
        {
            DateTime baseDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)(date.ToUniversalTime() - baseDate).TotalMilliseconds;
        }
    }

    public class Extensive
    {
        string json;
        SQL.Request request = new SQL.Request();
        Network network = new Network();
        Model.Base64ToImagesFile base64ToImagesFile = new Base64ToImagesFile();
        /* Шифрование в MD5 */
        public async Task<string> GetHashStringInMD5(string s)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(s));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /* Запись в базу при "Запомни меня" */
        public async Task SaveToBaseFromSaveMe(bool cheked)
        {
            var connecting = request.BD();
            var DataToBase = connecting.Get<SQL.Settings>(1);
            DataToBase.SaveMe = true;
            connecting.RunInTransaction(() =>
            {
                connecting.Update(DataToBase);
            });
        }

        /* Проверкас сервера */ 
        public async Task<string> Datatransfer(string url, NameValueCollection Data)
        {
            string StatusDataTransfer = await network.DataTransfer(url, Data);
            return StatusDataTransfer;
        }

        /* Обновление данных пользователя после авторизации */
        /*JsonToData*/
        public async Task<UserAccount> UserAccountJsonToData(string OutAuth)
        {
            UserAccount data = JsonConvert.DeserializeObject<UserAccount>(OutAuth); // Распаковка json
            return data;
        }
        /* Запись данных пользователя в базу (insert or update) */
        public async Task<bool> UserAccountWriteToBase(UserAccount data)
        {
            var connecting = request.BD();
            try
            {
                var quantity = connecting.Table<SQL.Profile>().Count();
                if (quantity == 0)
                {
                    var newData = new SQL.Profile
                    {
                        UserId = data.Id,
                        Login = data.Login,
                        Password = data.Password,
                        Name = data.Name,
                        FatherName = data.Father_name,
                        Surname = data.Surname,
                        Email = data.Email,
                        Platform = data.Platform,
                        Telephone = data.Phone,
                        Status = data.Status,
                        Token = data.Token,
                        Role = data.Rols
                    };
                    connecting.Insert(newData);
                }
                else
                {
                    var DataToBase = connecting.Get<SQL.Profile>(1);
                    DataToBase.UserId = data.Id;
                    DataToBase.Login = data.Login;
                    DataToBase.Password = data.Password;
                    DataToBase.Name = data.Name;
                    DataToBase.FatherName = data.Father_name;
                    DataToBase.Surname = data.Surname;
                    DataToBase.Email = data.Email;
                    DataToBase.Platform = data.Platform;
                    DataToBase.Telephone = data.Phone;
                    DataToBase.Status = data.Status;
                    DataToBase.Token = data.Token;
                    DataToBase.Role = data.Rols;
                    connecting.RunInTransaction(() =>
                    {
                        connecting.Update(DataToBase);
                    });
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

      

        /* Чистка профиля и данных после выхода из аккаунта или смены сервера */
        public async Task<bool> ClearProfileData()
        {
            try
            {
                var Connecting = request.BD();
                Connecting.DropTable<Model.SQL.Profile>();
                Connecting.CreateTable<Model.SQL.Profile>();
                Connecting.DeleteAll<Model.SQL.MeNearmiss>();
                Connecting.DeleteAll<Model.SQL.NearMissMy>();
                Connecting.DeleteAll<Model.SQL.Jobs>();
                Connecting.DeleteAll<Model.SQL.Platforms>();
                Connecting.DropTable<Model.SQL.Iniciator>();
                Connecting.DropTable<Model.SQL.Executor>();
                Connecting.DropTable<Model.SQL.Responsible>();
                Connecting.CreateTable<Model.SQL.Iniciator>();
                Connecting.CreateTable<Model.SQL.Executor>();
                Connecting.CreateTable<Model.SQL.Responsible>();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Получить все Near Miss при первом заходе Инициатора */
        public async Task<bool> GetPrimaryLoadNearMissMy ()
        {
            try
            {
                GetMyId getMyId = new GetMyId();
                var userid = getMyId.getMyId(); 
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "GetPrimaryLoadNearMissMy");
                Data.Add("userid", userid);
                var AnswerGetPrimaryLoadNearMissMy = await network.DataTransfer(Url, Data);
                if (AnswerGetPrimaryLoadNearMissMy != "")
                {
                    var data = JsonConvert.DeserializeObject<List<DloadNearMissMy>>(AnswerGetPrimaryLoadNearMissMy); // Распаковка json
                    var statusGetPrimaryLoadNearMissMyWriteToBase = await PrimaryLoadNearMissMyWriteToBase(data);
                    if (statusGetPrimaryLoadNearMissMyWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Записываем в базу все Neat Miss при первичном заходе инициатора */
        public async Task<bool> PrimaryLoadNearMissMyWriteToBase(List<DloadNearMissMy> data)
        {
            var connecting = request.BD();
            try
            {
                
               
                 foreach (var s in data)
                 {
                   // var idJob = connecting.Query<SQL.Jobs>("SELECT `id` FROM `Jobs` WHERE `Serverid`=" + s.job + "");
                    var idPlatform = connecting.Query<SQL.Platforms>("SELECT `id` FROM `Platforms` WHERE `Serverid`=" + s.platform + "");
                 //   foreach (var a in idJob)
                 //    {       
                        foreach (var b in idPlatform)
                        {

                            var insertData = new SQL.Iniciator
                            {
                                Id = s.id_tel,
                                IdServ = s.id,
                            //    Job = a.Id,
                                Violation = s.actionIn,
                                ActionTaken = s.actionDo,
                                Status = s.status+1,
                                Date = s.createDate,
                                Platform = b.Id,
                                Photo = base64ToImagesFile.GetImageFile(s.photo)
                            };
                            connecting.Insert(insertData);
                        }
               //      }
                 }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /* Получить все Near Miss при первом заходе ответственным */
        public async Task<bool> GetPrimaryLoadResponsible()
        {
            try
            {
                GetMyId getMyId = new GetMyId();
                var userid = getMyId.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "GetPrimaryLoadResponsible");
                Data.Add("userid", userid);
                var AnswerGetPrimaryLoadNearMissMy = await network.DataTransfer(Url, Data);
                if ((AnswerGetPrimaryLoadNearMissMy != "") && (AnswerGetPrimaryLoadNearMissMy != "[]") && (AnswerGetPrimaryLoadNearMissMy != null))
                {
                    var data = JsonConvert.DeserializeObject<List<DloadResponsible>>(AnswerGetPrimaryLoadNearMissMy); // Распаковка json
                    var statusGetPrimaryLoadNearMissMyWriteToBase = await PrimaryLoadResponsibleMyWriteToBase(data);
                    if (statusGetPrimaryLoadNearMissMyWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Записываем в базу все Neat Miss при первичном заходе ответственным */
        public async Task<bool> PrimaryLoadResponsibleMyWriteToBase(List<DloadResponsible> data)
        {
            var connecting = request.BD();
            try
            {
                foreach (var s in data)
                {
                    // var idJob = connecting.Query<SQL.Jobs>("SELECT `id` FROM `Jobs` WHERE `Serverid`=" + s.job + "");
               //     var idExecutor = connecting.Query<Model.SQL.ExecuterList>("SELECT `Id` FROM `ExecuterList` WHERE `ServerId`=" + s.id_responsible + "");
              //      foreach (var b in idExecutor)
              //      {
                        var insertData = new SQL.Responsible
                        {
                            Id = s.id,
                            Status = s.status + 1,
                            Violation = s.actionIn,
                            ActionTaken = s.actionDo,
                            Iniciator = s.iniciator_fio,
                            Platform = s.platform_name,
                            Date = s.createDate,
                            Executor = s.id_responsible,
                            DeadlineDate = s.deadlineDate,
                            Recomendation = s.demand,
                            Photo = base64ToImagesFile.GetImageFile(s.photo)
                        };
                        connecting.Insert(insertData);
                //    }
                    //      }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        /* Получить все Near Miss при первом заходе  исполнителем*/
        public async Task<bool> GetPrimaryLoadExecutor()
        {
            try
            {
                GetMyId getMyId = new GetMyId();
                var userid = getMyId.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "GetPrimaryLoadExecutor");
                Data.Add("userid", userid);
                var AnswerGetPrimaryLoadNearMissMy = await network.DataTransfer(Url, Data);
                if (AnswerGetPrimaryLoadNearMissMy != "")
                {
                    var data = JsonConvert.DeserializeObject<List<DloadExecutor>>(AnswerGetPrimaryLoadNearMissMy); // Распаковка json
                    var statusGetPrimaryLoadNearMissMyWriteToBase = await PrimaryLoadExecutorWriteToBase(data);
                    if (statusGetPrimaryLoadNearMissMyWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Записываем в базу все Neat Miss при первичном заходе исполнителем */
        public async Task<bool> PrimaryLoadExecutorWriteToBase(List<DloadExecutor> data)
        {
            var connecting = request.BD();
            try
            {


                foreach (var s in data)
                {
                    // var idJob = connecting.Query<SQL.Jobs>("SELECT `id` FROM `Jobs` WHERE `Serverid`=" + s.job + "");
                    //   var idPlatform = connecting.Query<SQL.Platforms>("SELECT `id` FROM `Platforms` WHERE `Serverid`=" + s.platform + "");
                    //   foreach (var a in idJob)
                    //    {       
                    //    foreach (var b in idPlatform)
                    //    {

                    var insertData = new SQL.Executor
                    {
                        Id = s.id,
                        Status = s.status + 1,
                        Violation = s.actionIn,
                        ActionTaken = s.actionDo,
                        Iniciator = s.iniciator_fio,
                        Responsible = s.responsible_fio,
                        Platform = s.platform_name,
                        Date = s.createDate,
                        Recomendations = s.demand,
                        DeadlineDate = s.deadlineDate,
                        Photo = base64ToImagesFile.GetImageFile(s.photo)
                    };
                    connecting.Insert(insertData);
                    //    }
                    //      }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }







    }
}
