﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace WellCompliance.Model
{
    class ConvertBase64
    {
        /* Конвертируем картинку в строку base64 */
        public string ConvertImageToBase64(string ImgPach) // Конвертация картинки в base64
        {
            string ImgBase64 = "data:image/jpeg;base64," + Convert.ToBase64String(File.ReadAllBytes(ImgPach));
            return ImgBase64;
        }
    }
}
