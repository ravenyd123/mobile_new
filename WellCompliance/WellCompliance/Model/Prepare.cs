﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WellCompliance.Model
{
    class Prepare
    {
        Model.SQL.Request request = new SQL.Request();
         /* Создаем таблицы */
        public bool PrepareTables()
        {
            var connecting = request.BD();
            try
            {
                connecting.CreateTable<SQL.NearMissMy>();
                connecting.CreateTable<SQL.MeNearmiss>();
                connecting.CreateTable<SQL.NearMissStatus>();
                connecting.CreateTable<SQL.Profile>();
                connecting.CreateTable<SQL.Jobs>();
                connecting.CreateTable<SQL.Platforms>();
                connecting.CreateTable<SQL.Settings>();
                connecting.CreateTable<SQL.ActionDo>();
                connecting.CreateTable<SQL.Tasks>();
                connecting.CreateTable<SQL.Iniciator>();
                connecting.CreateTable<SQL.Executor>();
                connecting.CreateTable<SQL.Responsible>();
                connecting.CreateTable<SQL.ExecuterList>();

                if (CreateDefaultSettings())
                {

                    return true;
                } else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

        /* Создаем таблицу с настройками и заполняем первоначальными значениями, если это необходимо */
        public bool CreateDefaultSettings()
        {
            var connecting = request.BD();
            try
            {
                var CountSettings = connecting.Table<SQL.Settings>().Count();
                if (CountSettings == 0)
                {
                    var newData = new SQL.Settings // вносим пустые данные в таблицу с настройками
                    {
                        Url = "",
                        Protocol = "https://",
                        Port = "443",
                        SaveMe = false,
                        HazardMy = "Мои NearMiss",
                        HazardToMe = "NearMiss мне",
                        Report1 = "Проверка 1 уровня",
                        Report2 = "Проверка 2 уровня",
                        Report3 = "Проверка 3 уровня",
                        Report4 = "Проверка 4 уровня"
                    };
                    connecting.Insert(newData);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

      

    }
}
