﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
namespace WellCompliance.Model
{
    class CompletingNM
    {

        Model.SQL.Request request = new Model.SQL.Request();
        Model.ConvertBase64 convertBase64 = new ConvertBase64();
        Model.Network network = new Model.Network();
        GetMyId myid = new GetMyId();
        string json;

        public async Task<bool>CompliteSend(int Id_NM, string WhatDoExecutor)
        {
            try
            {
                var Connecting = request.BD();

                var DataToBase = Connecting.Get<Model.SQL.Executor>(Id_NM);
                DataToBase.WhatDo = WhatDoExecutor;
                Connecting.RunInTransaction(() =>
                {
                    Connecting.Update(DataToBase);
                });

                var userid = myid.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "Complitenm");
                Data.Add("whatdo", WhatDoExecutor);
                Data.Add("userid", userid);
                Data.Add("id", Id_NM.ToString());
                var AnswerGetTasks = await network.DataTransfer(Url, Data);

                if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                    var ExecutorToBase = Connecting.Get<SQL.Executor>(Id_NM);
                    ExecutorToBase.Status = 5;
                    Connecting.RunInTransaction(() =>
                    {
                        Connecting.Update(ExecutorToBase);
                    });
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task<bool>SoglasSend(int Id_NM)
        {
            try
            {
                var Connecting = request.BD();
                var userid = myid.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/app/views/nearmiss/mobile.php";
                Data.Add("action", "SoglasSend");
                Data.Add("userid", userid);
                Data.Add("id", Id_NM.ToString());
                var AnswerGetTasks = await network.DataTransfer(Url, Data);

                if ((AnswerGetTasks != "") && (AnswerGetTasks != "[]") && (AnswerGetTasks != null))
                {
                    var ResponsibleToBase = Connecting.Get<SQL.Responsible>(Id_NM);
                    ResponsibleToBase.Status = 6;
                    Connecting.RunInTransaction(() =>
                    {
                        Connecting.Update(ResponsibleToBase);
                    });
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

       


    }
}
