﻿using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Net;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Collections.Specialized;



namespace WellCompliance.Model

{
    class LoadUserData
    {
        SQL.Request request = new SQL.Request();
        Model.Network network = new Model.Network();
        /* Получаем виды работ */
        public async Task<bool> GetJob()
        {
            try
            {
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/api/LoadData.php";
                Data.Add("action", "GetJobs");
                var AnswerGetJob = await network.DataTransfer(Url, Data);
                if (AnswerGetJob != "")
                {
                    var data = JsonConvert.DeserializeObject<List<JobsType>>(AnswerGetJob); // Распаковка json
                    var statusJobsWriteToBase = await JobsWriteToBase(data);
                    if (statusJobsWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Записываем в базу виды работ */
        public async Task<bool> JobsWriteToBase(List<JobsType> data)
        {
            var connecting = request.BD();
            try
            {
                connecting.DropTable<Model.SQL.Jobs>();
                connecting.CreateTable<Model.SQL.Jobs>();
                foreach (var s in data)
                {
                    var insertData = new SQL.Jobs
                    {
                        Serverid = s.Id,
                        JobsName = s.JobsName
                    };
                    connecting.Insert(insertData);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Получаем площадки */
        public async Task<bool> GetPlatforms()
        {
            try
            {
                GetMyId getMyId = new GetMyId();
                var userid = getMyId.getMyId();
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/api/LoadData.php";
                Data.Add("action", "GetPlatforms");
                
                var AnswerGetPlatforms = await network.DataTransfer(Url, Data);
                if (AnswerGetPlatforms != "")
                {
                    var data = JsonConvert.DeserializeObject<List<Platforms>>(AnswerGetPlatforms); // Распаковка json
                    var statusPlatformsWriteToBase = await PlatformsWriteToBase(data);
                    if (statusPlatformsWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        /* Записываем площадки */
        public async Task<bool> PlatformsWriteToBase(List<Platforms> data)
        {
            var connecting = request.BD();
            try
            {
                connecting.DropTable<Model.SQL.Platforms>();
                connecting.CreateTable<Model.SQL.Platforms>();
                foreach (var s in data)
                {
                    var insertData = new SQL.Platforms
                    {
                        Serverid = s.Id,
                        PlatformsName = s.PlatformName
                    };
                    connecting.Insert(insertData);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Получаем действия инициатора */
        public async Task<bool> GetActionDo()
        {
            try
            {
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/api/LoadData.php";
                Data.Add("action", "GetActionDoList");
                var AnswerGetActionDo = await network.DataTransfer(Url, Data);
                if (AnswerGetActionDo != "")
                {
                    var data = JsonConvert.DeserializeObject<List<ActionDo>>(AnswerGetActionDo); // Распаковка json
                    var statusActionDosWriteToBase = await GetActionDoWriteToBase(data);
                    if (statusActionDosWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Записываем действия инициатора */
        public async Task<bool> GetActionDoWriteToBase(List<ActionDo> data)
        {
            var connecting = request.BD();
            try
            {
                connecting.DropTable<Model.SQL.ActionDo>();
                connecting.CreateTable<Model.SQL.ActionDo>();

                foreach (var s in data)
                {
                    var insertData = new SQL.ActionDo
                    {
                        Id = s.Id,
                        Action = s.ActionDO
                    };
                    connecting.Insert(insertData);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /* Получаем статусы Near Miss */
        public async Task<bool> GetStatusNearMiss()
        {
            try
            {
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/api/LoadData.php";
                Data.Add("action", "GetStatusNearMiss");
                var AnswerGetStatusNearMiss = await network.DataTransfer(Url, Data);
                if (AnswerGetStatusNearMiss != "")
                {
                    var data = JsonConvert.DeserializeObject<List<statusNearMiss>>(AnswerGetStatusNearMiss); // Распаковка json
                    var statusStatusNearMissWriteToBase = await StatusNearMissWriteToBase(data);
                    if (statusStatusNearMissWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        /* Записываем Статусы Near Miss */
        public async Task<bool> StatusNearMissWriteToBase(List<statusNearMiss> data)
        {
            var connecting = request.BD();
            try
            {
                connecting.DropTable<Model.SQL.NearMissStatus>();
                connecting.CreateTable<Model.SQL.NearMissStatus>();
                foreach (var s in data)
                {
                    var insertData = new SQL.NearMissStatus
                    {
                        Id = s.Id,
                        NameStatus = s.status
                    };
                    connecting.Insert(insertData);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }




        /* Получаем список исполнителей */
        public async Task<bool> GetExecutorList()
        {
            try
            {
                var Data = new NameValueCollection();
                string Url = network.GetUrlServer() + "/api/LoadData.php";
                Data.Add("action", "GetExecutorList");
                var AnswerGetStatusNearMiss = await network.DataTransfer(Url, Data);
                if (AnswerGetStatusNearMiss != "")
                {
                    var data = JsonConvert.DeserializeObject<List<listExecutors>>(AnswerGetStatusNearMiss); // Распаковка json
                    var statusStatusNearMissWriteToBase = await StatusExecutorListToBase(data);
                    if (statusStatusNearMissWriteToBase)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        /* Записываем список исполнителей */
        public async Task<bool> StatusExecutorListToBase(List<listExecutors> data)
        {
            var connecting = request.BD();
            try
            {
                connecting.DropTable<Model.SQL.ExecuterList>();
                connecting.CreateTable<Model.SQL.ExecuterList>();
                foreach (var s in data)
                {
                    var insertData = new SQL.ExecuterList
                    {
                        ServerId = s.Id,
                        Fio = s.fio
                    };
                    connecting.Insert(insertData);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
