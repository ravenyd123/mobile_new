﻿using System;
using SQLite;

namespace WellCompliance.Model.SQL
{
    class Base
    {
    }
    /* Мною созданные NearMiss */
    [Table("NearMissMy")]
    public class NearMissMy
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } //ID 
        public int Job { get; set; } // Вид работ
        public string Violation { get; set; } // Нарушения и комментарии к месту
        public string ActionTaken { get; set; } // Какие действия приняты
        public int Status { get; set; } // Статус. На рассмотрении, выполнен, не выполнен, в работе
        public string Date { get; set; } // Дата обнаружения
        public string DateComplite { get; set; } // Дата исправления
        public string Geolocation { get; set; } // Координаты
        public int Platform { get; set; } // платформа
        public string Photo { get; set; } // фотки 
    }

    /* NearMiss для ответственного */
    [Table("MeNearmiss")]
    public class MeNearmiss
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } //ID 
        public int Job { get; set; } // Вид работ
        public string Violation { get; set; } // Нарушения и комментарии к месту
        public string ActionTaken { get; set; } // Какие действия приняты
        public int Status { get; set; } // Статус. На рассмотрении, выполнен, не выполнен, в работе
        public string Date { get; set; } // Дата обнаружения
        public string DateComplite { get; set; } // Дата исправления
        public string Geolocation { get; set; } // Координаты 
        public int Platform { get; set; } // платформа
        public string Photo { get; set; } // фотки 
    }

    /* Статусы NearMiss */
    [Table("NearMissStatus")]
    public class NearMissStatus
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } // id статуса
        public string NameStatus { get; set; } // Название статуса

    }

    /* Профиль текущего пользователя */
    [Table("Profile")]
    public class Profile
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Password2 { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public string Email { get; set; }
        public string Platform { get; set; }
        public string Telephone { get; set; }
        public int Status { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
    }

    /* Виды работ */
    [Table("Jobs")]
    public class Jobs
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } // id вида работ
        public int Serverid { get; set; } // id на сервере
        public string JobsName { get; set; } // Название вида работ
    }

    /* Площадки */
    [Table("Platforms")] // Платформы
    public class Platforms
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } // id цеха, Платформы
        public int Serverid { get; set; } // id на сервере
        public string PlatformsName { get; set; } // Название цеха, платформы
    }

    /* Настройки */
    [Table("Settings")] // Настройки
    public class Settings
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } // ID 
        public string Url { get; set; } // Адрес сервера подключения
        public string Protocol { get; set; } // протокол http:// или https://
        public string Port { get; set; } // Порт сервера подключения
        public bool SaveMe { get; set; } // Кнопка "Сохранить меня"
        public string HazardToMe { get; set; } // Название нарушения, которое адресовано мне
        public string HazardMy { get; set; } // Название нарушение, которое я нашел
        public string Report1 { get; set; } // Название отчета 1 уровня
        public string Report2 { get; set; } // Название отчета 2 уровня
        public string Report3 { get; set; } // Название отчета 3 уровня
        public string Report4 { get; set; } // Название отчета 4 уровня

    }

    /* Список что инициатор может сделать */
    [Table("ActionDo")]
    public class ActionDo
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } // ID 
        public string Action { get; set; } // Адрес сервера подключения


    }

    /* Задачи */
    [Table("Tasks")]
    public class Tasks
    {
        [PrimaryKey]
        public int Id { get; set; } //ID 
        public int Platform { get; set; } // площадка
        public string Status { get; set; } // Статус задачи
        public string Date { get; set; } // Дата создания задачи
        public string Deadline { get; set; } // Срок задачи
        public string Finished { get; set; } // Дата выполнения задачи
        public string Name { get; set; } // Тема
        public string Description { get; set; } // Описание 
        public int Director { get; set; } // Постановщик задачи 
        public int Responsible { get; set; } // Постановщик задачи 
    }

    /*Инициатор*/
    [Table("Iniciator")]
    public class Iniciator
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } //ID 
        public int IdServ { get; set; } //ID на сервере
        public string Violation { get; set; } // Нарушения и комментарии к месту
        public string ActionTaken { get; set; } // Какие действия приняты
        public int Status { get; set; } // Статус. На рассмотрении, выполнен, не выполнен, в работе
        public string Date { get; set; } // Дата обнаружения
        public string DateComplite { get; set; } // Дата исправления
        public int Platform { get; set; } // платформа
        public string Photo { get; set; } // фотки 
    }

    [Table("Executor")]
    public class Executor
    {
        [PrimaryKey]
        public int Id { get; set; } //ID 
        public string Violation { get; set; } // Нарушения и комментарии к месту
        public string ActionTaken { get; set; } // Какие действия приняты
        public string Recomendations { get; set; } // Что нужно сделать
        public int Status { get; set; } // Статус
        public string Date { get; set; } // Дата обнаружения
        public string DateComplite { get; set; } // Дата исправления
        public string DateValidation { get; set; } // Дата исправления
        public string DeadlineDate { get; set; } // Срок устранения
        public string Iniciator { get; set; } // Инициатор
        public string Responsible { get; set; } // Ответственный
        public string Platform { get; set; } // платформа
        public string Photo { get; set; } // адресс картинки
        public string WhatDo { get; set; } // Что сделал исполнитель
        public string ReasonOfInconsistency { get; set; } // Причина несогласования

    }
    [Table("Responsible")]
    public class Responsible
    {
        [PrimaryKey]
        public int Id { get; set; } //ID 
        public string Iniciator { get; set; } // Инициатор
        public string Violation { get; set; } // Нарушения и комментарии к месту
        public string ActionTaken { get; set; } // Какие действия приняты
        public string Date { get; set; } // Дата обнаружения
        public string Platform { get; set; } // платформа
        public int Status { get; set; } // Статус
        public string Photo { get; set; } // адресс картинки
        public string DeadlineDate { get; set; } // Дата обнаружения
        public int Executor { get; set; } // id исполнителя
        public string Recomendation { get; set; } // Что необходимо сделать для устранения
        public string WhatDo { get; set; } // Что сделал исполнитель
        public string ReasonOfInconsistency { get; set; } // Причина несогласования

    }

    [Table("ExecuterList")]
    public class ExecuterList
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } //ID 
        public int ServerId{ get; set; } // id исполнителя на серваке
        public string Fio { get; set; } // фио исполнителя
       
    }

}
