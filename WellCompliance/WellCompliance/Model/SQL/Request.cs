﻿using System;
using System.IO;
using SQLite;

using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Diagnostics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using WellCompliance.ViewModel;
using WellCompliance.Model;
using System.Collections.Specialized;
using Xamarin.Essentials;
using Xamarin.Forms.PlatformConfiguration;

namespace WellCompliance.Model.SQL
{
    class Request
    {
         string dbFileName = Path.Combine(FileSystem.AppDataDirectory, "wcbase.db3"); // Полный путь к файлу базы
                                                                                                                        //  string dbFileName = Path.Combine("wcbase.doc"); // FILE NAME TO USE WHEN COPIED
        /* Читаем файл базы. Если файла базы нет, то мы его создаем */

           public SQLiteConnection BD()
        {
            if (!File.Exists(dbFileName))
            {
                File.Create(dbFileName);
            }

            SQLiteConnection connection = new SQLiteConnection(dbFileName);
            return connection;
        }

        /* Проверка существования базы */
        public bool ChekExistBD()
        {
            if (File.Exists(dbFileName))
            {
                return true;

            } else
            {
                return false;
            }
        }

        /* Создание базы */
        public bool CreateBD()
        {

            try
            {
                File.Create(dbFileName);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
