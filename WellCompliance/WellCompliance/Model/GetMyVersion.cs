﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace WellCompliance.Model
{
    class GetMyVersion
    {
        /* Версия приложения */
        public string App()
        {
            string buildNumber = AppInfo.VersionString;
            return buildNumber;
        }

        /* Производитель */
        public string Manufacturer()
        {           
            string manufacturer = DeviceInfo.Manufacturer; // Manufacturer (Samsung)
            return manufacturer;
        }

        /* Модель устройства */
        public string Model()
        {
            string device = DeviceInfo.Model; // Device Model (SMG-950U, iPhone10,6)
            return device;
        }

        /* Название ОС, платформа */
        public string Platform()
        {
            string platform = DeviceInfo.Platform.ToString();  // Platform (Android)
            return platform;
        }

        /* Версия ОС, платформы */
        public string VerOS()
        {
            string version = DeviceInfo.VersionString;  // Operating System Version Number(7.0)
            return version;
        }

    }
}
