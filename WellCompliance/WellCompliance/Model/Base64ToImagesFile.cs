﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Essentials;
using Xamarin.Forms.PlatformConfiguration;

namespace WellCompliance.Model
{
    class Base64ToImagesFile
    {
        string documentsFolder;
        string filepath;
        Model.RandomKey randomKey = new RandomKey();
    
        public string GetImageFile(string database64)
        {
            //  string base64 = database64.Remove(0,23);
            //  documentsFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            //   string filepath =  Path.Combine(FileSystem.CacheDirectory, randomKey.GenerateKey() + ".jpg");
            //  documentsFolder = "well";
            //  filepath = Path.Combine(documentsFolder, randomKey.GenerateKey() + ".jpg");
            //    var base64EncodedBytes = System.Convert.FromBase64String(base64);
            //     var decoded = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            //  var decoded = "12312312321312321312321321321321321321";
            //    File.WriteAllText(filepath, decoded);
            //    return filepath;
            //  MediaStore.Images.Media.InsertImage(this, yourBitmap, yourTitle, yourDescription);
            if (database64 != null)
            {
                try
                {
                    string FileAddress = Path.Combine(FileSystem.AppDataDirectory, randomKey.GenerateKey() +".jpg");

                    string base64str = database64.Substring(database64.IndexOf(',') + 1);
                    byte[] bytes = Convert.FromBase64String(base64str);
                        File.WriteAllBytes(FileAddress, bytes);
                        return FileAddress;
  

             //       string path = @"/storage/sdcard0/";
            //        string subpath = @"Wellcompliance";
             //       DirectoryInfo dirInfo = new DirectoryInfo(path);
             //       if (!dirInfo.Exists)
            //        {
            //            dirInfo.Create();
            //        }
            //        dirInfo.CreateSubdirectory(subpath);
            //        string strpath = "/storage/sdcard0/Wellcompliance/" + randomKey.GenerateKey() + ".jpg";
           //         string base64str = database64.Substring(database64.IndexOf(',') + 1);
           // /       byte[] bytes = Convert.FromBase64String(base64str);
          //          File.WriteAllBytes(strpath, bytes);
           //         return strpath;
               }
                catch (Exception)
                {
                   return "";
                }
            } else
            {
                return "";
            }
        }
    }
}
