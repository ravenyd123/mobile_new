﻿using System;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace WellCompliance.Model
{
    class Network
    {
        SQL.Request request = new SQL.Request();
        GetMyVersion getMyVersion = new GetMyVersion();

        /* Получаем адрес сервера */
        public string GetUrlServer()
        {
            var connecting = request.BD();
            var DataToBase = connecting.Get<SQL.Settings>(1);
            string Url = DataToBase.Url;
            string Protocol = DataToBase.Protocol;
            string Port = DataToBase.Port;
            string FullAddres = Protocol + Url + ":" + Port;
            return FullAddres;
        }

        /* Преобразование ответа из байта в string */
        private async Task<string> EncodingText(byte[] Data)
        {
            return System.Text.Encoding.UTF8.GetString(Data);
        }

        /* Сам метод отправки */
        public async Task<string> DataTransfer(string Url, NameValueCollection Data)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    GetMyId getMyId = new GetMyId();
                    var userid = getMyId.getMyId();
                    Console.WriteLine("my id");
                    Console.WriteLine(userid);
                    webClient.Headers.Add(HttpRequestHeader.Cookie, "userid=" + userid);
                    // webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                    webClient.Headers.Add( "user-agent", "ver. " + getMyVersion.App() + ", Platform " + getMyVersion.VerOS());
                    //  webClient.Headers.Add(HttpRequestHeader.Cookie, "cookiename=cookievalue");
                    Data.Add("userid", userid);
                    Data.Add("versionapp", getMyVersion.App());
                    Data.Add("manufacturer", getMyVersion.Manufacturer());
                    Data.Add("model", getMyVersion.Model());
                    Data.Add("platform", getMyVersion.Platform());
                    Data.Add("versionos", getMyVersion.VerOS());
                    var response = await webClient.UploadValuesTaskAsync(Url, Data);
                    Console.WriteLine("hqhq");
                    Console.WriteLine(response);
                    string Answer = await EncodingText(response);
                    Console.WriteLine(Answer);
                    return Answer;
                }
            }
           catch (Exception)
            {
               return "";
            }
        }

        public string SaveNMAndSendToServer(string json)
        {
            GetMyId getMyId = new GetMyId();
            var userid = getMyId.getMyId();
            var Url = GetUrlServer();
            string url = Url + "/app/views/nearmiss/mobile.php";
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add(HttpRequestHeader.Cookie, "userid=" + userid);
                webClient.Headers.Add("user-agent", "ver. " + getMyVersion.App() + ", Platform " + getMyVersion.VerOS());
                //  webClient.Headers.Add("user-agent", poVersion);
                var pars = new NameValueCollection();
                pars.Add("userid", userid);
                pars.Add("versionapp", getMyVersion.App());
                pars.Add("manufacturer", getMyVersion.Manufacturer());
                pars.Add("model", getMyVersion.Model());
                pars.Add("platform", getMyVersion.Platform());
                pars.Add("versionos", getMyVersion.VerOS());
                pars.Add("action", "SaveNMAndSendToServer");
                pars.Add("data", json);
                var response = webClient.UploadValues(url, pars);
                string str = System.Text.Encoding.UTF8.GetString(response);
                return str;
            }
        }

        public string SaveNMAndSendToServer2(string json)
        {
            GetMyId getMyId = new GetMyId();
            var userid = getMyId.getMyId();
            var Url = GetUrlServer();
            string url = Url + "/app/views/nearmiss/mobile.php";
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add(HttpRequestHeader.Cookie, "userid=" + userid);
                webClient.Headers.Add("user-agent", "ver. " + getMyVersion.App() + ", Platform " + getMyVersion.VerOS());
                var pars = new NameValueCollection();
                pars.Add("userid", userid);
                pars.Add("versionapp", getMyVersion.App());
                pars.Add("manufacturer", getMyVersion.Manufacturer());
                pars.Add("model", getMyVersion.Model());
                pars.Add("platform", getMyVersion.Platform());
                pars.Add("versionos", getMyVersion.VerOS());
                pars.Add("action", "SaveNMAndSendToServer2");
                pars.Add("data", json);
                var response = webClient.UploadValues(url, pars);
                string str = System.Text.Encoding.UTF8.GetString(response);
                return str;
            }
        }

        public string SaveNMAndSendToServer3(string json)
        {
            GetMyId getMyId = new GetMyId();
            var userid = getMyId.getMyId();
            var Url = GetUrlServer();
            string url = Url + "/app/views/nearmiss/mobile.php";
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add(HttpRequestHeader.Cookie, "userid=" + userid);
                webClient.Headers.Add("user-agent", "ver. " + getMyVersion.App() + ", Platform " + getMyVersion.VerOS());
                //  webClient.Headers.Add("user-agent", poVersion);
                var pars = new NameValueCollection();
                pars.Add("userid", userid);
                pars.Add("versionapp", getMyVersion.App());
                pars.Add("manufacturer", getMyVersion.Manufacturer());
                pars.Add("model", getMyVersion.Model());
                pars.Add("platform", getMyVersion.Platform());
                pars.Add("versionos", getMyVersion.VerOS());
                pars.Add("action", "SaveNMAndSendToServer3");
                pars.Add("data", json);
                var response = webClient.UploadValues(url, pars);
                string str = System.Text.Encoding.UTF8.GetString(response);
                return str;
            }
        }
    }
}
