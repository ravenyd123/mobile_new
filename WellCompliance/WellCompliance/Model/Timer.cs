﻿using System;
using System.Collections.Generic;
using System.Text;
using WellCompliance.ViewModel;

namespace WellCompliance.Model
{
   public class Timer
    {
        Notification notification = new Notification();
        OnlineDataVerification onlineDataVerification = new OnlineDataVerification();
        LocalNotifications localNotifications = new LocalNotifications();
        GetMyId getMyId = new GetMyId();
        //int a = 0;
        /* Метод произходящий по таймеру */
        public void Message(object sender, System.Timers.ElapsedEventArgs e)
        {
            // a = a+20;
            var ab = getMyId.getMyId();
            Console.WriteLine("Проверка userid");
            Console.WriteLine(ab);
            if (getMyId.getMyId() != "0")
                
            {
                onlineDataVerification.DataVerification();
                Console.WriteLine("Старт переменной службы");
            }
            else
            {
                Console.WriteLine("Отмена старта. Нет значения userid");
            }
        }

         /* Таймер на проверку новых данных на сервере*/
         public void AutoInquiry()
         {
             var Timer = new System.Timers.Timer(20000);
             Timer.AutoReset = true;
             Timer.Enabled = true;
             Timer.Elapsed += new System.Timers.ElapsedEventHandler(Message);
             Timer.Start();
         }
    }
}
