﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WellCompliance.Model
{
    class PrimaryDloadNM
    {
        Model.Extensive extensive = new Model.Extensive();
        public async System.Threading.Tasks.Task<bool> PrimaryDloadNearMissAsync()
        {
            var AnswerPrimaryDloadMyNearMiss = await extensive.GetPrimaryLoadNearMissMy();
            if (AnswerPrimaryDloadMyNearMiss)
            {
                //  var AnswerPrimaryDloadNearMissToMe = await extensive.GetPrimaryLoadNearMissToMe();
                //   if (AnswerPrimaryDloadNearMissToMe)
                //   {
                //       return true;
                //    }
                //    else
                //    {
                //        return false;
                //     }

                return true;
            } 
            else
            {
                return false;
            }

        }

        public async System.Threading.Tasks.Task<bool> PrimaryDloadNearMissExecutorAsync()
        {
            var AnswerPrimaryDloadMyNearMiss = await extensive.GetPrimaryLoadExecutor();
            if (AnswerPrimaryDloadMyNearMiss)
            {
                //  var AnswerPrimaryDloadNearMissToMe = await extensive.GetPrimaryLoadNearMissToMe();
                //   if (AnswerPrimaryDloadNearMissToMe)
                //   {
                //       return true;
                //    }
                //    else
                //    {
                //        return false;
                //     }

                return true;
            }
            else
            {
                return false;
            }

        }

        public async System.Threading.Tasks.Task<bool> PrimaryDloadNearMissResponsibleAsync()
        {
            var AnswerPrimaryDloadMyNearMiss = await extensive.GetPrimaryLoadResponsible();
            if (AnswerPrimaryDloadMyNearMiss)
            {
                //  var AnswerPrimaryDloadNearMissToMe = await extensive.GetPrimaryLoadNearMissToMe();
                //   if (AnswerPrimaryDloadNearMissToMe)
                //   {
                //       return true;
                //    }
                //    else
                //    {
                //        return false;
                //     }

                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
