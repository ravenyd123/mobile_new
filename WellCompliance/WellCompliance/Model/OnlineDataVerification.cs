﻿using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Collections.Specialized;
using Xamarin.Forms;
using WellCompliance.ViewModel;

namespace WellCompliance.Model
{
    public class OnlineDataVerification
    {
        ViewModel.Notification alert = new ViewModel.Notification();
        SendTasks sendTasks = new SendTasks();
        public async void DataVerification()
        {
            Console.WriteLine("Старт проверки");
            await sendTasks.NewNM(); // Новые NM для ответственных
            await sendTasks.NewNMForExecutor(); // Новые NM для исполнителей
            await sendTasks.NewStatusAndPush(); // остальные пуши со статусами
        }

    }
}
